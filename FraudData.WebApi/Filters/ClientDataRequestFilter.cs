﻿using FraudData.WebApi.Models;
using Starbucks.OpenApi.ServiceExtensions.Exceptions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Http.Filters;

namespace FraudData.WebApi.Filters
{
    public class ClientDataRequestFilter : ActionFilterAttribute
    {
        public override void OnActionExecuting(System.Web.Http.Controllers.HttpActionContext actionContext)
        {
            var actionArguments = actionContext.ActionArguments;
            var transactionRequest = actionArguments.ContainsKey("clientData") ? actionArguments["clientData"] as ClientData : null;

            if (transactionRequest == null)
            {
                throw new ApiException(HttpStatusCode.BadRequest, FraudApiErrorResource.InvalidRequestCode, FraudApiErrorResource.InvalidRequestMessage);
            }

            base.OnActionExecuting(actionContext);
        }

    }
}