﻿using FraudData.WebApi.Models;
using Starbucks.OpenApi.ServiceExtensions.Exceptions;
using System.Net;
using System.Web.Http.Filters;

namespace FraudData.WebApi.Filters
{
    public class TransactionRequestFilter : ActionFilterAttribute
    {
        public override void OnActionExecuting(System.Web.Http.Controllers.HttpActionContext actionContext)
        {
            var actionArguments = actionContext.ActionArguments;
            var transactionRequest = actionArguments.ContainsKey("setTransactionContextRequest") ? actionArguments["setTransactionContextRequest"] as SetTransactionContextRequest : null;

            if (transactionRequest == null)
            {
                throw new ApiException(HttpStatusCode.BadRequest, FraudApiErrorResource.InvalidRequestCode, FraudApiErrorResource.InvalidRequestMessage);
            }

            base.OnActionExecuting(actionContext);
        }
    }
}