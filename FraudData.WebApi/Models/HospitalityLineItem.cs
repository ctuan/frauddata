﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;

namespace FraudData.WebApi.Models
{
    
    [DataContract(Name = "hospitalityLineItem", Namespace = Constants.ContractNamespace)]
    public class HospitalityLineItem
    {
        [DataMember(Name = "commonLineItem", IsRequired = true, Order = 0)]
        public CommonLineItem CommonLineItem { get; set; }

        [DataMember(Name = "arrivalDate", IsRequired = false, Order = 1)]
        public DateTime arrivalDate { get; set; }

        [DataMember(Name = "departureDate", IsRequired = false, Order = 2)]
        public DateTime DepartureDate { get; set; }

        [DataMember(Name = "numberOfAdults", IsRequired = false, Order = 3)]
        public int numberOfAdults { get; set; }

        [DataMember(Name = "numberOfChildren", IsRequired = false, Order = 4)]
        public int NumberOfChildren { get; set; }

        [DataMember(Name = "facilityRating", IsRequired = false, Order = 5)]
        public HospitalityLineItemFacilityRatingEnum FacilityRating { get; set; }

        [DataMember(Name = "roomCapacity", IsRequired = false, Order = 6)]
        public int RoomCapacity { get; set; }
   
    }
}
