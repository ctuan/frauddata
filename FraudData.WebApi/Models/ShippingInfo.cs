﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;

namespace FraudData.WebApi.Models
{
    [DataContract(Name = "shippingInfo", Namespace = Constants.ContractNamespace)]
    public class ShippingInfo
    {
        [DataMember(Name = "firstName", IsRequired = false, Order = 0)]
        public string FirstName { get; set; }

        [DataMember(Name = "lastName", IsRequired = false, Order = 1)]
        public string LastName { get; set; }

        [DataMember(Name = "baseAddress", IsRequired = false, Order = 2)]
        public BaseAddress BaseAddress { get; set; }

        [DataMember(Name = "newAddress", IsRequired = false, Order = 3)]
        public bool NewAddress { get; set; }

        [DataMember(Name = "createdAt", IsRequired = false, Order = 1)]
        public DateTime? createdAt { get; set; }

        [DataMember(Name = "previousChargeback", IsRequired = false, Order = 1)]
        public bool PreviousChargeback { get; set; }

    }
}
