﻿using Starbucks.FraudData.Proxy.Common.Models;

namespace Starbucks.FraudData.WebApi.Models
{
    public class ApplicationReply : IApplicationReply
    {
        public string RCode { get; set; }

        public string RFlag { get; set; }

        public string RMsg { get; set; }

        public string Name { get; set; }
    }
}
