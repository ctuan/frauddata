﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;

namespace FraudData.WebApi.Models
{
    [DataContract(Name = "lineItems", Namespace = Constants.ContractNamespace)]
    public class LineItem
    {
        [DataMember(Name = "retailLineItem", IsRequired = false, Order = 0)]
        public RetailLineItem RetailLineItem { get; set; }
    }
}
