﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Web;

namespace FraudData.WebApi.Models
{
     [DataContract(Name = "SetTransactionContextResponse", Namespace = Constants.ContractNamespace)]
    public class SetTransactionContextResponse
    {
        /// <summary>
        ///  Application level acknowledgement code.
        /// </summary>
        [DataMember(Name = "responseEnvelope", IsRequired = true, Order = 0)]
         public ResponseEnvelope ResponseEnvelope { get; set; }

        /// <summary>
        ///   A unique ID that you specify to track the payment. NOTE: You are responsible for ensuring that the ID is unique.
        /// </summary>
        [DataMember(Name = "trackingId", IsRequired = true, Order = 1)]
        public string TrackingId { get; set; }

        /// <summary>
        ///  Contains information about the sender's account
        /// </summary>
        [DataMember(Name = "senderAccount", IsRequired = false, Order = 2)]
        public Account SenderAccount { get; set; }

        /// <summary>
        ///  Order information for this transaction.
        /// </summary>
        [DataMember(Name = "subOrders", IsRequired = false, Order = 3)]
        public SubOrder SubOrder { get; set; }

        /// <summary>
        /// IP address for this transaction.
        /// </summary>
        [DataMember(Name = "ipAddress", IsRequired = false, Order = 4)]
        public IpAddress IpAddress { get; set; }

        /// <summary>
        /// A list of merchant specific data for the transaction.
        /// </summary>
        [DataMember(Name = "additionalData", IsRequired = false, Order = 5)]
        public IList<Pair> AdditionalData { get; set; }
    }
}