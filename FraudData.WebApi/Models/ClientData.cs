﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Web;

namespace FraudData.WebApi.Models
{
    [DataContract]
    public class ClientData
    {
        [DataMember(IsRequired = true, Order = 0)]
        public string UserId { get; set; }
        [DataMember(IsRequired = false, Order = 1)]
        public string CardId { get; set; }
        [DataMember(IsRequired = false, Order = 2)]
        public string OrderSource { get; set; }
        [DataMember(IsRequired = false, Order = 3)]
        public string PurchaserIpAddress { get; set; }
        [DataMember(IsRequired = false, Order = 4)]
        public string PaymentMethodNickName { get; set; }
        [DataMember(IsRequired = false, Order = 5)]
        public bool IsAutoReload { get; set; }
        [DataMember(IsRequired = false, Order = 6)]
        public string PaypalPayerId { get; set; }
        [DataMember(IsRequired = false, Order = 7)]
        public decimal PurchaserSessionDuration { get; set; }
        [DataMember(IsRequired = false, Order = 8)]
        public ClientMDDData[] ClientMDDData { get; set; }
    }
}