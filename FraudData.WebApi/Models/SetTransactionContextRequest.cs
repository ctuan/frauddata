﻿using System.Runtime.Serialization;

namespace FraudData.WebApi.Models
{
    [DataContract(Name = "SetTransactionContextRequest", Namespace = Constants.ContractNamespace)]
    public class SetTransactionContextRequest
    {
        /// <summary>
        ///  A request envelope has generic fields that are common to every request. Each operation request contains a request envelope, 
        ///  in addition to the payload that is specific to the operation.
        /// </summary>
        [DataMember(Name = "requestEnvelope", IsRequired = false, Order = 0)]
        public RequestEnvelope RequestEnvelope { get; set; }

        /// <summary>
        ///   A unique ID that you specify to track the payment. NOTE: You are responsible for ensuring that the ID is unique.
        /// </summary>
        [DataMember(Name = "trackingId", IsRequired = true, Order = 1)]
        public string TrackingId { get; set; }

        /// <summary>
        ///  Contains information about the sender's account
        /// </summary>
        [DataMember(Name = "senderAccount", IsRequired = false, Order = 2)]
        public Account SenderAccount { get; set; }


        /// <summary>
        ///  Order information for this transaction.
        /// </summary>
        [DataMember(Name = "subOrders", IsRequired = false, Order = 3)]
        public SubOrder[] SubOrders { get; set; }
        
        /// <summary>
        /// IP address for this transaction.
        /// </summary>
        [DataMember(Name = "ipAddress", IsRequired = false, Order = 4)]
        public IpAddress IpAddress { get; set; }

        /// <summary>
        /// A list of merchant specific data for the transaction.
        /// </summary>
        [DataMember(Name = "additionalData", IsRequired = false, Order = 5)]
        public Pair[] AdditionalData { get; set; }
    }
}