﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;

namespace FraudData.WebApi.Models
{
    [DataContract(Name = "ticketingLineItemTypeEnum")]
    public enum TicketingLineItemTypeEnum
    {
        [EnumMember]
        CONCERT,
        [EnumMember]
        CONFERENCE,
        [EnumMember]
        SPORTS,
        [EnumMember]
        OTHER
    }
}
