﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;

namespace FraudData.WebApi.Models
{
    [DataContract(Name = "device", Namespace = Constants.ContractNamespace)]
    public class Device
    {
        [DataMember(Name = "id", IsRequired = false, Order = 0)]
        public string Id { get; set; }

        [DataMember(Name = "lastSeenIpAddress", IsRequired = false, Order = 1)]
        public string LastSeenIpAddress { get; set; }

        [DataMember(Name = "type", IsRequired = false, Order = 2)]
        public DeviceTypeEnum Type { get; set; }

        [DataMember(Name = "operatingSystem", IsRequired = false, Order = 3)]
        public string OperatingSystem { get; set; }

        [DataMember(Name = "userAgent", IsRequired = false, Order = 4)]
        public string UserAgent { get; set; }

        [DataMember(Name = "firstSeen", IsRequired = false, Order = 5)]
        public DateTime FirstSeen { get; set; }

        [DataMember(Name = "lastSeen", IsRequired = false, Order = 6)]
        public DateTime LastSeen { get; set; }

        [DataMember(Name = "seenCount", IsRequired = false, Order = 7)]
        public int SeenCount { get; set; }

    }
}
