﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;

namespace FraudData.WebApi.Models
{
    [DataContract(Name = "telcoLineItem", Namespace = Constants.ContractNamespace)]
    public class TelcoLineItem
    {
        [DataMember(Name = "commonLineItem", IsRequired = true, Order = 0)]
        public CommonLineItem CommonLineItem { get; set; }

        [DataMember(Name = "phoneNumber", IsRequired = false, Order = 1)]
        public string phoneNumber { get; set; }

        [DataMember(Name = "origin", IsRequired = false, Order = 2)]
        public TelcoLineItemOriginEnum Origin { get; set; }

        [DataMember(Name = "resellerID", IsRequired = false, Order = 2)]
        public string ResellerID { get; set; }


    }
}
