﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;

namespace FraudData.WebApi.Models
{
    [DataContract(Name = "commonLineItem", Namespace = Constants.ContractNamespace)]
    public class CommonLineItem
    {
        [DataMember(Name = "identifier", IsRequired = false, Order = 0)]
        public string Identifier { get; set; }

        [DataMember(Name = "itemPrice", IsRequired = true, Order = 1)]
        public decimal ItemPrice { get; set; }

        [DataMember(Name = "currencyCode", IsRequired = true, Order = 2)]
        public string CurrencyCode { get; set; }

        [DataMember(Name = "itemCount", IsRequired = false, Order = 3)]
        public int ItemCount { get; set; }

        [DataMember(Name = "name", IsRequired = false, Order = 4)]
        public string Name { get; set; }

         [DataMember(Name = "category", IsRequired = false, Order = 5)]
        public string Category { get; set; }

         [DataMember(Name = "subCategory", IsRequired = false, Order = 6)]
        public string SubCategory { get; set; }

         [DataMember(Name = "description", IsRequired = false, Order = 6)]
         public string Description { get; set; }
    
    }
}
