﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;

namespace FraudData.WebApi.Models
{
    [DataContract(Name = "travelLineItemTypeEnum")]
    public enum TravelLineItemTypeEnum
    {
        [EnumMember]
        AIR,
        [EnumMember]
        TRAIN,
        [EnumMember]
        OTHER
    }
}
