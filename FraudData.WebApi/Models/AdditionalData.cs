﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace FraudData.WebApi.Models
{
    [DataContract(Name = "additionalData", Namespace = Constants.ContractNamespace)]
    public class AdditionalData
    {
        [DataMember(Name = "pair", IsRequired = false, Order = 0)]
        public Pair[] Pairs { get; set; }
    }
}
