﻿using Starbucks.FraudData.Proxy.Common.Models;
using System.Collections.Generic;

namespace Starbucks.FraudData.WebApi.Models
{
    public class Report : IReport
    {
        public IEnumerable<IRequest> Requests { get; set; }

        public string Name { get; set; }

        public string Version { get; set; }

        public string MerchantID { get; set; }

        public string ReportStartDate { get; set; }

        public string ReportEndDate { get; set; }
    }
}
