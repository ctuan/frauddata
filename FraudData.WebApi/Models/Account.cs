﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;

namespace FraudData.WebApi.Models
{
    [DataContract(Name = "account", Namespace = Constants.ContractNamespace)]
    public class Account
    {
        /// <summary>
        ///  Unique identifier for a Paypal account
        /// </summary>
        [DataMember(Name = "paypalAccountID", IsRequired = true, Order = 0)]
        public PaypalAccountIdentifier PaypalAccountID { get; set; }

        /// <summary>
        /// A Partner's view of the user with this paypalAccountID
        /// </summary>
        [DataMember(Name = "partnerAccount", IsRequired = true, Order = 1)]
        public PartnerAccount PartnerAccount { get; set; }
    }
}
