﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;

namespace FraudData.WebApi.Models
{
    [DataContract(Name = "keyValuePairs", Namespace = Constants.ContractNamespace)]
    public class KeyValuePairs
    {
        [DataMember(Name = "pair", IsRequired = false, Order = 0)]
        public Pair Pair { get; set; }

    }
}
