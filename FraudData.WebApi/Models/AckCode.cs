﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;

namespace FraudData.WebApi.Models
{
    [DataContract(Name = "ackCode")]
    public enum AckCode
    {
        [EnumMember]
        Success,
        [EnumMember]
        Failure,
        [EnumMember]
        Warning,
        [EnumMember]
        SuccessWithWarning,
        [EnumMember]
        FailureWithWarning,
        [EnumMember]
        CustomCode
    }
}
