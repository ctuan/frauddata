﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;

namespace FraudData.WebApi.Models
{
    [DataContract(Name = "subOrders", Namespace = Constants.ContractNamespace)]
    public class SubOrder
    {
        [DataMember(Name = "lineItems", IsRequired = false, Order = 0)]
        public LineItem[] LineItems { get; set; }
    }
}
