﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;

namespace FraudData.WebApi.Models
{
    [DataContract(Name = "partnerAccount", Namespace = Constants.ContractNamespace)]
    public class PartnerAccount
    {
        [DataMember(Name = "email", IsRequired = false, Order = 0)]
        public string email { get; set; }

        [DataMember(Name = "phone", IsRequired = false, Order = 1)]
        public string Phone { get; set; }

        [DataMember(Name = "firstName", IsRequired = false, Order = 2)]
        public string FirstName { get; set; }

        [DataMember(Name = "lastName", IsRequired = false, Order = 3)]
        public string LastName { get; set; }


        [DataMember(Name = "createDate", IsRequired = false, Order = 4)]
        public DateTime CreateDate { get; set; }

        /// <summary>
        /// The last date of a valid transaction. The transaction need not be a PayPal transaction.
        /// </summary>
        [DataMember(Name = "lastGoodTransactionDate", IsRequired = false, Order = 5)]
        public DateTime LastGoodTransactionDate { get; set; }

        /// <summary>
        /// Transaction count in the last three months for this partner account.
        /// </summary>
        [DataMember(Name = "transactionCountThreeMonths", IsRequired = false, Order = 6)]
        public int TransactionCountThreeMonths { get; set; }
    }
}
