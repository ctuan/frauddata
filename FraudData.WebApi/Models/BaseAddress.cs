﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;

namespace FraudData.WebApi.Models
{
    [DataContract(Name = "baseAddress", Namespace = Constants.ContractNamespace)]
    public class BaseAddress
    {
        [DataMember(Name = "line1", IsRequired = false, Order = 0)]
        public string Line1 { get; set; }

        [DataMember(Name = "line2", IsRequired = false, Order = 1)]
        public string Line2 { get; set; }

        [DataMember(Name = "city", IsRequired = false, Order = 2)]
        public string City { get; set; }

        [DataMember(Name = "state", IsRequired = false, Order = 3)]
        public string State { get; set; }

        [DataMember(Name = "postCode", IsRequired = false, Order = 4)]
        public string PostCode { get; set; }

        [DataMember(Name = "countryCode", IsRequired = false, Order = 5)]
        public string CountryCode { get; set; } 

    }
}
