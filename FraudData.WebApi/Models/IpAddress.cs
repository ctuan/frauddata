﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;

namespace FraudData.WebApi.Models
{
    [DataContract(Name = "ipAddress", Namespace = Constants.ContractNamespace)]
    public class IpAddress
    {
        [DataMember(Name = "ipAddress", IsRequired = true, Order = 0)]
        public string IPAddress { get; set; }

    }
}
