﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;

namespace FraudData.WebApi.Models
{
    [DataContract(Name = "travelLineItem", Namespace = Constants.ContractNamespace)]
    public class TravelLineItem
    {
        [DataMember(Name = "commonLineItem", IsRequired = true, Order = 0)]
        public CommonLineItem CommonLineItem { get; set; }

        [DataMember(Name = "type", IsRequired = false, Order = 1)]
        public TravelLineItemTypeEnum type { get; set; }

        [DataMember(Name = "origin", IsRequired = false, Order = 2)]
        public string Origin { get; set; }

        [DataMember(Name = "destination", IsRequired = false, Order = 3)]
        public string Destination { get; set; }

         [DataMember(Name = "departureDate", IsRequired = false, Order = 4)]
        public DateTime DepartureDate { get; set; }

        [DataMember(Name = "operator", IsRequired = false, Order = 5)]
        public string Operator { get; set; }

        [DataMember(Name = "tripIdentifier", IsRequired = false, Order = 6)]
        public string TripIdentifier { get; set; }

        [DataMember(Name = "travelClass", IsRequired = false, Order = 7)]
        public TravelLineItemClassEnum TravelClass { get; set; }

        [DataMember(Name = "passengers", IsRequired = false, Order = 8)]
        public IList<PartnerAccount> Passengers { get; set; }

         [DataMember(Name = "reason", IsRequired = false, Order = 9)]
        public TravelLineItemReasonEnum reason { get; set; }

          [DataMember(Name = "bookingType", IsRequired = false, Order = 10)]
        public TravelLineItemBookingTypeEnum BookingType { get; set; }
   
    }
}
