﻿using Starbucks.FraudData.Proxy.Common.Models;

namespace Starbucks.FraudData.WebApi.Models
{
    public class PaymentData : IPaymentData
    {
        public string PaymentRequestID { get; set; }

        public string PaymentProcessor { get; set; }

        public string Amount { get; set; }

        public string CurrencyCode { get; set; }

        public string TotalTaxAmount { get; set; }

        public string AuthorizationCode { get; set; }

        public string AVSResult { get; set; }

        public string AVSResultMapped { get; set; }
    }
}
