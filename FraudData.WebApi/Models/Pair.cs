﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;

namespace FraudData.WebApi.Models
{
    [DataContract(Name = "pair", Namespace = Constants.ContractNamespace)]
    public class Pair
    {
        [DataMember(Name = "key", IsRequired = false, Order = 0)]
        public string Key { get; set; }

        [DataMember(Name = "value", IsRequired = false, Order = 1)]
        public string Value { get; set; }

    }
}
