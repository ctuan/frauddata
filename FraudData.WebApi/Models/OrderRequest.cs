﻿using FraudData.WebApi.Models;
using Starbucks.FraudData.Proxy.Common.Models;
using System.Runtime.Serialization;

namespace Starbucks.FraudData.WebApi.Models
{
    [DataContract]
    public class OrderRequest : IOrderRequest
    {
        [DataMember]
        public string MerchantId { get; set; }

        [DataMember]
        public string MerchantReferenceNumber { get; set; }

        /// <summary>
        /// currently a constant value 'transactionDetail'
        /// </summary>
        [DataMember]
        public string SubType { get; set; }

        /// <summary>
        /// if provided, must be of format YYYYMMDD
        /// </summary>
        [DataMember]
        public string TargetDate { get; set; }

        /// <summary>
        /// A constant value 'transaction'
        /// </summary>
        [DataMember]
        public string Type { get; set; }

        [DataMember]
        public string RequestId { get; set; }

        public string Username { get; set; }

        public string VersionNumber { get; set; }

        public string Password { get; set; }
    }
}