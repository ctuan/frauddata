﻿using Starbucks.FraudData.Proxy.Common.Models;
using System.Collections.Generic;

namespace Starbucks.FraudData.WebApi.Models
{
    public class Profile : IProfile
    {
        public string ProfileDecision { get; set; }

        public string ProfileMode { get; set; }

        public IEnumerable<IProfileRule> RuleList { get; set; }

        public string Name { get; set; }
    }
}
