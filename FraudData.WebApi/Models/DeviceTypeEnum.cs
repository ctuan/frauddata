﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;

namespace FraudData.WebApi.Models
{
    [DataContract(Name = "deviceTypeEnum")]
    public enum DeviceTypeEnum
    {
        [EnumMember]
        BROWSER,
        [EnumMember]
        PC_NATIVE,
        [EnumMember]
        MOBILE_NATIVE
    }
}
