﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;

namespace FraudData.WebApi.Models
{
    [DataContract(Name = "deliveryEnum")]
    public enum DeliveryEnum
    {
        [EnumMember]
        INSTANT_DOWNLOAD,
        [EnumMember]
        STREAMING,
        [EnumMember]
        EMAIL_DOWNLOAD,
        [EnumMember]
        SAME_DAY,
        [EnumMember]
        OVERNIGHT,
        [EnumMember]
        STANDARD
    }
}
