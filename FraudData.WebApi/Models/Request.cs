﻿using Starbucks.FraudData.Proxy.Common.Models;
using System.Collections.Generic;

namespace Starbucks.FraudData.WebApi.Models
{
    public class Request : IRequest
    {
        public IBillTo BillTo { get; set; }

        public IShipTo ShipTo { get; set; }

        public IEnumerable<IPaymentMethod> PaymentMethod { get; set; }

        public IEnumerable<ILineItem> LineItems { get; set; }

        public IEnumerable<IApplicationReply> ApplicationReplies { get; set; }

        public IPaymentData PaymentData { get; set; }

        public IMerchantDefinedData MerchantDefinedData { get; set; }

        public IRiskData RiskData { get; set; }

        public IEnumerable<IProfile> ProfileList { get; set; }

        public string MerchantReferenceNumber { get; set; }

        public string RequestDate { get; set; }

        public string RequestID { get; set; }

        public string SubscriptionID { get; set; }

        public string Source { get; set; }

        public string TransactionReferenceNumber { get; set; }

        public string PredecessorRequestID { get; set; }
    }
}
