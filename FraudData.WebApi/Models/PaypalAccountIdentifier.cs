﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;

namespace FraudData.WebApi.Models
{
    [DataContract(Name = "paypalAccountIdentifier", Namespace = Constants.ContractNamespace)]
    public class PaypalAccountIdentifier
    {
        /// <summary>
        /// The PayPal payer ID, which uniquely identifies a PayPal account.
        /// </summary>
        [DataMember(Name = "payerId", IsRequired = false, Order = 0)]
        public string PayerId { get; set; }
    }
}
