﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;

namespace FraudData.WebApi.Models
{
   [DataContract(Name = "responseEnvelope", Namespace = Constants.ContractNamespace)]
    public class ResponseEnvelope
    {
       [DataMember(Name = "timestamp", IsRequired = false, Order = 0)]
        public DateTime Timestamp{get;set;} 

        [DataMember(Name = "ack", IsRequired = false, Order = 1)]
        public AckCode Ack{get;set;}

        [DataMember(Name = "correlationId", IsRequired = false, Order = 2)]
        public string CorrelationId { get; set; }

        [DataMember(Name = "build", IsRequired = false, Order = 3)]
        public string Build { get; set; }

        [DataMember(Name = "other", IsRequired = false, Order = 4)]
        public object Other { get; set; } 

    }
}
