﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;

namespace FraudData.WebApi.Models
{
    [DataContract(Name = "dgLineItemTypeEnum")]
    public enum DgLineItemLicenseTypeEnum
    {  
        [EnumMember]
        RENT,
        [EnumMember]
        PURCHASE        
    }
}
