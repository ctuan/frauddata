﻿using Starbucks.FraudData.Dal.Common.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace FraudData.WebApi.Models
{
    public class FraudCheckData : IFraudCheckData
    {

        public int AgeOfAccountSinceCreation { get; set; }

        public decimal BalancePriorToReload { get; set; }

        public int BirthDay { get; set; }

        public int BirthMonth { get; set; }

        public string CallCenterRepId { get; set; }

        public decimal CardBalance { get; set; }

        public DateTime? CardRegistrationDate { get; set; }

        public string Currency { get; set; }

        public bool FacebookSignUp { get; set; }

        public bool IsAutoreload { get; set; }

        public bool IsCardCurrentlyRegisteredToUser { get; set; }

        public bool IsDefaultSvcCard { get; set; }

        public bool IsPartner { get; set; }

        public bool IsReloadDoneRecently { get; set; }

        public bool IsUserSignedIn { get; set; }

        public IMDDField[] MDDFields { get; set; }

        public bool MailSignUp { get; set; }

        public string Nickname { get; set; }

        public string OrderSource { get; set; }

        public string PaypalPayerId { get; set; }

        public string PurchaserIpAddress { get; set; }

        public decimal PurchaserSessionDuration { get; set; }

        public string RewardTierLevel { get; set; }

        public string SessionId { get; set; }

        public string ShippingMethod { get; set; }

        public string SvcNumber { get; set; }

        public string SvcNumberReloaded { get; set; }

        public bool TextMessageSignUp { get; set; }

        public int? TimeSinceLastPOSTransaction { get; set; }

        public int TotalCardsRegistered { get; set; }

        public bool TwitterSignUp { get; set; }

        public string UserId { get; set; }

        public string UserName { get; set; }

        public bool eMailSignUp { get; set; }

        public int? DaysSinceLastReload { get; set; }

        public string EmailAddress { get; set; }
    }
}