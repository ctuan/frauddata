﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;

namespace FraudData.WebApi.Models
{
    [DataContract(Name = "travelLineItemClassEnum")]
    public enum TravelLineItemClassEnum
    {
        [EnumMember]
        FIRST,
        [EnumMember]
        BUSINESS,
        [EnumMember]
        ECONOMY
    }
}
