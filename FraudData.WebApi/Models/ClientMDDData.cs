﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Web;

namespace FraudData.WebApi.Models
{
    [DataContract]
    public class ClientMDDData
    {
        [DataMember]
        public string ID { get; set; }
        [DataMember]
        public string Value { get; set; }
    }
}