﻿using Starbucks.FraudData.Proxy.Common.Models;

namespace Starbucks.FraudData.WebApi.Models
{
    public class ShipTo : IShipTo
    {
        public string Phone { get; set; }
    }
}
