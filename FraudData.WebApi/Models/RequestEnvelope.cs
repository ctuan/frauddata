﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;

namespace FraudData.WebApi.Models
{
    [DataContract(Name = "requestEnvelope", Namespace = Constants.ContractNamespace)]
    public class RequestEnvelope
    {
        [DataMember(Name = "errorLanguage", IsRequired = false, Order = 0)]
        public string ErrorLanguage { get; set; }
    }
}
