﻿using Starbucks.FraudData.Proxy.Common.Models;

namespace Starbucks.FraudData.WebApi.Models
{
    public class ProfileRule : IProfileRule
    {
        public string RuleName { get; set; }

        public string RuleDecision { get; set; }
    }
}
