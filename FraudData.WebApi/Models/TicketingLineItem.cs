﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;

namespace FraudData.WebApi.Models
{
    [DataContract(Name = "ticketingLineItem", Namespace = Constants.ContractNamespace)]
    public class TicketingLineItem
    {
        [DataMember(Name = "commonLineItem", IsRequired = true, Order = 0)]
        public CommonLineItem CommonLineItem { get; set; }

        [DataMember(Name = "startDate", IsRequired = false, Order = 1)]
        public DateTime startDate { get; set; }

        [DataMember(Name = "endDate", IsRequired = false, Order = 2)]
        public DateTime EndDate { get; set; }

        [DataMember(Name = "address", IsRequired = false, Order = 3)]
        public BaseAddress Address { get; set; }

        [DataMember(Name = "type", IsRequired = false, Order = 4)]
        public TicketingLineItemTypeEnum Type { get; set; }

        [DataMember(Name = "attendees", IsRequired = false, Order = 5)]
        public IList<PartnerAccount> attendees { get; set; }

        [DataMember(Name = "subscription", IsRequired = false, Order = 6)]
        public bool Subscription { get; set; }
    }
}
