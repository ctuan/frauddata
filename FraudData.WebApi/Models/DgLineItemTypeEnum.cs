﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;

namespace FraudData.WebApi.Models
{
    [DataContract(Name = "dgLineItemTypeEnum")]
    public enum DgLineItemTypeEnum
    {
        [EnumMember]
        APPLICATION,
        [EnumMember]
        IN_APPLICATION_PURCHASE,
        [EnumMember]
        PLATFORM_CURRENCY,
        [EnumMember]
        MUSIC,
        [EnumMember]
        VIDEO,
        [EnumMember]
        LITERATURE,
        [EnumMember]
        OTHER
    }
}
