﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;

namespace FraudData.WebApi.Models
{
    [DataContract(Name = "travelLineItemReasonEnum")]
    public enum TravelLineItemReasonEnum
    {
        [EnumMember]
        BUSINESS,
        [EnumMember]
        LEISURE
    }
}
