﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;

namespace FraudData.WebApi.Models
{
    [DataContract(Name = "dgLineItem", Namespace = Constants.ContractNamespace)]
    public class DgLineItem
    {
        /// <summary>
        /// Common item fields for all types of product.
        /// </summary>
        [DataMember(Name = "commonLineItem", IsRequired = true, Order = 0)]
        public CommonLineItem CommonLineItem { get; set; }

        [DataMember(Name = "type", IsRequired = false, Order = 1)]
        public DgLineItemTypeEnum Type { get; set; }

        [DataMember(Name = "targetAudienceMinAge", IsRequired = false, Order = 2)]
        public int targetAudienceMinAge { get; set; }

        [DataMember(Name = "targetAudienceMaxAge", IsRequired = false, Order = 3)]
        public int targetAudienceMaxAge { get; set; }

        [DataMember(Name = "licenseType", IsRequired = false, Order = 4)]
        public DgLineItemLicenseTypeEnum DgLineItemLicenseTypeEnum { get; set; }
    }
}
