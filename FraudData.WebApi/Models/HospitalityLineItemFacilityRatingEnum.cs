﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;

namespace FraudData.WebApi.Models
{
    [DataContract(Name = "hospitalityLineItemFacilityRatingEnum")]
    public enum HospitalityLineItemFacilityRatingEnum
    {
        [EnumMember]
        ONE = 1,
        [EnumMember]
        TWO = 2,
        [EnumMember]
        THREE = 3,
        [EnumMember]
        FOUR = 4,
        [EnumMember]
        FIVE = 5
    }
}
