﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Routing;
using System.Web.Mvc;

namespace FraudData.WebApi
{
    public static class WebApiConfig
    {
        public static void Register(HttpConfiguration config)
        {
            config.Routes.MapHttpRoute(
              name: "GetUserDataWithCard",
              routeTemplate: "users/{userId}/cards/{cardId}/data",
              defaults: new { controller = "FraudData", action = "GetUserData" },
              constraints: new { httpMethod = new HttpMethodConstraint(HttpMethod.Post) }
              );

            config.Routes.MapHttpRoute(
             name: "GetUserData",
             routeTemplate: "users/{userId}/data",
             defaults: new { controller = "FraudData", action = "GetUserData" },
             constraints: new { httpMethod = new HttpMethodConstraint(HttpMethod.Post) }
             );

            config.Routes.MapHttpRoute(
             name: "SetTransactionContext",
             routeTemplate: "users/{userId}/set-transaction-context",
             defaults: new { controller = "PayPalRiskAssessment", action = "SetTransactionContext" },
             constraints: new { httpMethod = new HttpMethodConstraint(HttpMethod.Post) }
             );

            config.Routes.MapHttpRoute
            (
                name: "GetOrderDetail",
                routeTemplate: "OrderReport/GetOrderDetail",
                defaults: new { controller = "OrderReport", action = "GetOrderDetail" },
                 constraints: new { httpMethod = new HttpMethodConstraint(HttpMethod.Post) }
             );
        }
    }
}
