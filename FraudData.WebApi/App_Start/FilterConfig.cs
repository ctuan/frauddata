﻿using Starbucks.OpenApi.Logging.Common;
using Starbucks.OpenApi.ServiceExtensions.Filters;
using System.Web;
using System.Web.Http;
using System.Web.Http.Filters;
using System.Web.Mvc;

namespace FraudData.WebApi
{
    public class FilterConfig
    {
        public static void RegisterGlobalFilters(HttpFilterCollection filters)
        {
            var logRepository = GlobalConfiguration.Configuration.DependencyResolver.GetService(typeof(ILogRepository)) as ILogRepository;
            filters.Add(new ApiExceptionFilter(logRepository));
        }
    }
}