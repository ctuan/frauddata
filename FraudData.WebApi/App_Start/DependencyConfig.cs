﻿using Autofac;
using Autofac.Integration.WebApi;
using RiskAssessment.Provider;
using Starbucks.FraudData.CyberSource.Provider;
using Starbucks.FraudData.Dal.Common;
using Starbucks.FraudData.Dal.Sql;
using Starbucks.FraudData.Dal.Sql.Configuration;
using Starbucks.FraudData.Provider;
using Starbucks.FraudData.Provider.Common;
using Starbucks.OpenApi.Logging.Common;
using Starbucks.Rewards.Dal.Common;
using Starbucks.Rewards.Provider;
using Starbucks.Rewards.Provider.Common;
using Starbucks.TransactionHistory.Dal.Common;
using Starbucks.TransactionHistory.Provider;
using Starbucks.TransactionHistory.Provider.Common;
using Starbucks.TransactionHistory.Provider.Configuration;
using System.Configuration;
using System.Reflection;
using System.Web.Http;
using Starbucks.OpenApi.Logging.EnterpriseLibrary;
using Starbucks.LogCounter.Provider.Common;
using Starbucks.LogCounter.Provider;
using Starbucks.FraudData.Proxy.Configuration;
using Starbucks.FraudData.Proxy;
using Starbucks.FraudData.Proxy.Common;
using FraudData.WebApi.Configuration;

namespace FraudData.WebApi
{
    public class DependencyConfig
    {
        public static void Configure(HttpConfiguration httpConfiguration)
        {
            var builder = new ContainerBuilder();
            builder.RegisterApiControllers(Assembly.GetExecutingAssembly());
            builder.RegisterType<CyberSourceFraudDataProvider>().As<ICyberSourceFraudDataProvider>();
            builder.RegisterType<CyberSourceFraudDataDal>().As<ICyberSourceFraudDataDal>();
            builder.Register(c => ConfigurationManager.GetSection("fraudDataDalSettings") as FraudDataDalSqlSettingsSection)
                 .As<FraudDataDalSqlSettingsSection>();


            builder.RegisterType<LogRepository>().As<ILogRepository>().As<LogRepository>();
            builder.Register<ILogCounterManager>(c => LogCounterManager.Instance());

            builder.RegisterType<Starbucks.TransactionHistory.Dal.Sql.TransactionHistorySqlDataProvider>()
                 .As<ITransactionHistoryDataProvider>().WithParameter(new PositionalParameter(0, "TransactionHistory")).WithParameter(new PositionalParameter(1, 1000)).WithParameter(new PositionalParameter(2, 1000));
            builder.RegisterType<TransactionHistoryProvider>().As<ITransactionHistoryProvider>();
            builder.Register(
                p =>
                new TransactionHistoryProvider(
                    p.Resolve<ITransactionHistoryDataProvider>(), TransactionHistoryProviderSettings.Settings)).As<ITransactionHistoryProvider>();

            builder.RegisterType<RewardsProvider>().As<IRewardsProvider>();
            builder.RegisterType<Starbucks.Rewards.Dal.LoyaltyService.LoyaltyDal>().As<IRewardsDal>();
            builder.RegisterType<PayPalRiskAssessmentProvider>().As<IPayPalFraudDataProvider>();
            builder.RegisterType<PayPalFraudCheckData>().As<IPayPalFraudCheckData>();

            builder.Register(
              p =>
              new FraudDataProvider(
                  p.Resolve<ICyberSourceFraudDataProvider>())).As<IFraudDataProvider>();

            builder.Register(section => ConfigurationManager.GetSection("paymentInstrumentUrlSettings") as PaymentInstrumentSettingsSection).As<PaymentInstrumentSettingsSection>();
            builder.Register(section => ConfigurationManager.GetSection("fraudDataProviderSettings") as FraudDataProviderSettings).As<FraudDataProviderSettings>();
            builder.RegisterType<PaymentInstrumentServiceProxy>().As<IPaymentInstrumentServiceProxy>();
            builder.RegisterType<PaymentInstrumentServiceProvider>().As<IPaymentInstrumentServiceProvider>();
            builder.Register(section => ConfigurationManager.GetSection("orderRequestSetting") as OrderRequestSettingsSection).As<IOrderRequestSettingsSection>();
            var container = builder.Build();
            var resolver = new AutofacWebApiDependencyResolver(container);
            // httpConfiguration.DependencyResolver = resolver;

            GlobalConfiguration.Configuration.DependencyResolver = resolver;
        }
    }
}