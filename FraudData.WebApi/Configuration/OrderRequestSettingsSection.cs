﻿using System.Configuration;

namespace FraudData.WebApi.Configuration
{
    public interface IOrderRequestSettingsSection
    {
        OrderRequestSettingCollection Settings { get; set; }
    }

    public class OrderRequestSettingsSection : ConfigurationSection, IOrderRequestSettingsSection
    {
        [ConfigurationProperty("settings", IsDefaultCollection = true, IsRequired = true)]
        [ConfigurationCollection(typeof(OrderRequestSettingCollection),
            AddItemName = "add",
            ClearItemsName = "clear",
            RemoveItemName = "remove")]
        public OrderRequestSettingCollection Settings
        {
            get
            {
                return this["settings"] as OrderRequestSettingCollection;
            }
            set
            {
                this["settings"] = value;
            }
        }
    }
}