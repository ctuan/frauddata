﻿using System.Configuration;

namespace FraudData.WebApi.Configuration
{
    public class OrderRequestSettingElement : ConfigurationElement
    {
        public OrderRequestSettingElement()
        {

        }

        public OrderRequestSettingElement(string version, string username, string password)
        {
            PaymentInstrumentVersion = version;
            PaymentInstrumentUsername = username;
            PaymentInstrumentPassword = password;

        }

        [ConfigurationProperty("paymentInstrumentVersion", IsRequired = true, IsKey = false)]
        public string PaymentInstrumentVersion
        {
            get { return this["paymentInstrumentVersion"] as string; }
            set { this["paymentInstrumentVersion"] = value; }
        }

        [ConfigurationProperty("paymentInstrumentUsername", IsRequired = true, IsKey = false)]
        public string PaymentInstrumentUsername
        {
            get { return this["paymentInstrumentUsername"] as string; }
            set { this["paymentInstrumentUsername"] = value; }
        }

        [ConfigurationProperty("paymentInstrumentPassword", IsRequired = true, IsKey = false)]
        public string PaymentInstrumentPassword
        {
            get { return this["paymentInstrumentPassword"] as string; }
            set { this["paymentInstrumentPassword"] = value; }
        }

        [ConfigurationProperty("merchantKey", IsRequired = true, IsKey = true)]
        public string MerchantKey
        {
            get { return this["merchantKey"] as string; }
            set { this["merchantKey"] = value; }
        }
    }
}