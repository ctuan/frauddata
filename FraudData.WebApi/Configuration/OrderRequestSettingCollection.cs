﻿using System;
using System.Configuration;
using System.Linq;

namespace FraudData.WebApi.Configuration
{
    [ConfigurationCollection(typeof(OrderRequestSettingElement))]
    public class OrderRequestSettingCollection : ConfigurationElementCollection
    {
        protected override ConfigurationElement CreateNewElement()
        {

            return new OrderRequestSettingElement();
        }

        public ConfigurationElement this[int index]
        {
            get
            {
                return BaseGet(index) as ConfigurationElement;
            }
            set
            {
                if (BaseGet(index) != null)
                {
                    BaseRemoveAt(index);
                }
                BaseAdd(index, value);
            }
        }

        protected override object GetElementKey(ConfigurationElement element)
        {
            return (element as OrderRequestSettingElement).MerchantKey;
        }

        public void Remove(OrderRequestSettingElement orderRequestSetting)
        {
            BaseRemove(orderRequestSetting.PaymentInstrumentUsername);
        }

        public void Add(OrderRequestSettingElement orderRequestSetting)
        {
            BaseAdd(orderRequestSetting);
        }

        public void Clear()
        {
            BaseClear();
        }

        public void RemoveAt(int index)
        {
            BaseRemoveAt(index);
        }

        public void Remove(string name)
        {
            BaseRemove(name);
        }

        public OrderRequestSettingElement GetOrderRequestSetting(string key)
        {
            return this.OfType<OrderRequestSettingElement>().FirstOrDefault(x => String.Equals(x.MerchantKey, key, StringComparison.OrdinalIgnoreCase));
        }
    }
}