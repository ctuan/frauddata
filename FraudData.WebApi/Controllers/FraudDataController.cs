﻿using FraudData.WebApi.Filters;
using FraudData.WebApi.Models;
using Starbucks.FraudData.Provider.Common;
using Starbucks.OpenApi.ServiceExtensions.Exceptions;
using System;
using System.Net;
using System.Net.Http;

namespace FraudData.WebApi.Controllers
{
    public class FraudDataController : BaseController
    {
        private readonly ICyberSourceFraudDataProvider _fraudDataProvider;

        public FraudDataController(ICyberSourceFraudDataProvider fraudDataProvider)
        {
            _fraudDataProvider = fraudDataProvider;
            if (_fraudDataProvider == null)
            {
                throw new ArgumentNullException("fraudDataProvider");
            }
        }

        [System.Web.Http.HttpPost]
        [System.Web.Http.ActionName("GetUserData")]
        [ClientDataRequestFilter]
        public HttpResponseMessage GetUserDataForCyberSource(ClientData clientData)
        {
            IncrementLogCounter();
            if (clientData.UserId == null)
                throw new ApiException(HttpStatusCode.BadRequest, FraudApiErrorResource.InvalidRequestCode, FraudApiErrorResource.InvalidRequestMessage);

            Starbucks.FraudData.Provider.Common.Models.IFraudCheckData data;
            try
            {
                data = _fraudDataProvider.GetUserDataForFraudCheck(clientData.ToProvider());
            }
            catch (Exception ex)
            {
                IncrementLogCounterError();
                throw GetApiException(ex);
            }
            if (data == null)
            {
                throw new ApiException(HttpStatusCode.NotFound, FraudApiErrorResource.GeneralNotFoundCode,
                                       FraudApiErrorResource.GeneralNotFoundMessage);
            }
            HttpResponseMessage h = Request.CreateResponse(HttpStatusCode.OK, data.ToApi());
            return h;
        }
    }
}
