﻿using System.Collections.Generic;
using System.Configuration;
using System.Diagnostics;
using FraudData.WebApi.Filters;
using Microsoft.Practices.EnterpriseLibrary.Logging;
using Microsoft.Practices.ObjectBuilder2;
using OAuth2.Provider.Web;
using RiskAssessment.Provider.Models;
using Starbucks.Common.ResourceMapping;
using Starbucks.FraudData.Provider;
using Starbucks.FraudData.Provider.Common;
using Starbucks.FraudData.Provider.Common.ErrorResources;
using Starbucks.FraudData.Provider.Common.Exceptions;
using Starbucks.FraudData.Provider.Common.Models;
using Starbucks.OpenApi.ServiceExtensions.Exceptions;
using System;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using payPalAssessment = RiskAssessment.Provider;
using WebApModels = FraudData.WebApi.Models;

namespace FraudData.WebApi.Controllers
{
    public class PayPalRiskAssessmentController : BaseController
    {
        private readonly IPayPalFraudDataProvider _payPalRiskProvider;

        public PayPalRiskAssessmentController(IPayPalFraudDataProvider payPalRiskProvider)
        {
            #region FraudDataProviderException
            ResourceMap.Map((new ResourceMapItem(typeof(FraudDataProviderException), FraudDataProviderErrors.InvalidUserIdCode,
                            FraudApiErrorResource.InvalidUserIdCode, FraudApiErrorResource.InvalidUserIdMessage,
                            HttpStatusCode.BadRequest)))
                            .Map((new ResourceMapItem(typeof(FraudDataProviderException), FraudDataProviderErrors.UnableToGetRiskAssessmentScoresCode,
                            FraudApiErrorResource.UnableToGetRiskAssessmentScoresCode, FraudApiErrorResource.UnableToGetRiskAssessmentScoresMessage,
                            HttpStatusCode.BadRequest)))
                    .Map((new ResourceMapItem(typeof(FraudDataProviderException), FraudDataProviderErrors.TrackingIdCannotBeNullCode,
                            FraudApiErrorResource.TrackingIdCannotBeNullCode, FraudApiErrorResource.TrackingIdCannotBeNullMessage,
                            HttpStatusCode.BadRequest)));
            #endregion

            _payPalRiskProvider = payPalRiskProvider;
        }

        [HttpPost]
        [ActionName("SetTransactionContext")]
        [TransactionRequestFilter]
        [ApiOAuth2UserId]
        public async Task<HttpResponseMessage> SetTransactionContext(string userId, WebApModels.SetTransactionContextRequest setTransactionContextRequest)
        {
            // Adding validation
            if (setTransactionContextRequest.RequestEnvelope == null)
            {
                setTransactionContextRequest.RequestEnvelope = new WebApModels.RequestEnvelope();
            }
            if (setTransactionContextRequest.IpAddress == null)
            {
                setTransactionContextRequest.IpAddress = new WebApModels.IpAddress();
            }
            if (setTransactionContextRequest.SubOrders == null)
            {
                setTransactionContextRequest.SubOrders = new WebApModels.SubOrder[1];
                setTransactionContextRequest.SubOrders[0] = new WebApModels.SubOrder();
                setTransactionContextRequest.SubOrders[0].LineItems = new WebApModels.LineItem[0];

            }

            var request = new TransactionRequest
            {
                RequestEnvelope = new RequestEnvelope
                {
                    ErrorLanguage = setTransactionContextRequest.RequestEnvelope.ErrorLanguage
                },
                TrackingId = setTransactionContextRequest.TrackingId,
                IpAddress = new IpAddress { IPAddress = setTransactionContextRequest.IpAddress.IPAddress },
                SenderAccount = new payPalAssessment.Models.Account
                {
                    PaypalAccountId = new PaypalAccountIdentifier()
                },
                SubOrders = new ISubOrder[setTransactionContextRequest.SubOrders.Length]
            };

            var orders = setTransactionContextRequest.SubOrders.ToList().ConvertAll(Converters.ConvertToProviderSubOrder);
            request.SubOrders = orders.ToArray();

            var settings = ConfigurationManager.GetSection("fraudDataProviderSettings") as FraudDataProviderSettings;
            if (settings != null && settings.PayPalSettings.LogFraudCheckDataForPayPal)
            {
                LogHelper.LogPayPalRequest(userId, request);
            }

            ITransactionResponse response;
            try
            {
                Stopwatch payPalRiskWatch = new Stopwatch();
                payPalRiskWatch.Start();
                response = await _payPalRiskProvider.TransactionContext(userId, request);
                payPalRiskWatch.Stop();
                LogHelper.LogTimer(payPalRiskWatch, "PayPal Risk");

            }
            catch (AggregateException aex)
            {
                LogHelper.LogErrors(aex);
                IncrementLogCounterError();
                throw GetApiException(aex.InnerException);
            }
            catch (Exception ex)
            {
                LogHelper.WriteException(ex);
                IncrementLogCounterError();
                throw GetApiException(ex);
            }

            if (response == null)
            {
                throw new ApiException(HttpStatusCode.NotFound, FraudApiErrorResource.GeneralNotFoundCode,
                                       FraudApiErrorResource.GeneralNotFoundMessage);
            }

            if (settings != null && settings.PayPalSettings.LogFraudCheckDataForPayPal)
            {
                LogHelper.LogPayPalDetails(userId, response);
            }
            return Request.CreateResponse(HttpStatusCode.OK, response.ToApiResponse());
        }
    }
}
