﻿
using System;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using FraudData.WebApi.Configuration;
using Starbucks.FraudData.Provider.Common;
using Starbucks.FraudData.Proxy.Common.Models;
using Starbucks.FraudData.WebApi.Models;
using Starbucks.OpenApi.ServiceExtensions.Exceptions;

namespace FraudData.WebApi.Controllers
{
    public class OrderReportController : BaseController
    {
        private readonly IPaymentInstrumentServiceProvider _paymentInstrumentProvider;
        private readonly IOrderRequestSettingsSection _orderRequestSettingsSection;

        public OrderReportController(IPaymentInstrumentServiceProvider paymentInstrumentProvider, IOrderRequestSettingsSection orderRequestSettingSection)
        {
            if (paymentInstrumentProvider == null)
            {
                throw new ArgumentNullException("paymentInstrumentProvider", "cannot be null");

            }
            _paymentInstrumentProvider = paymentInstrumentProvider;
            if (orderRequestSettingSection == null)
            {
                throw new ArgumentNullException("orderRequestSettingSection", "cannot be null");
            }
            _orderRequestSettingsSection = orderRequestSettingSection;
        }

        [HttpPost]
        public async Task<HttpResponseMessage> GetOrderDetail(OrderRequest orderRequest)
        {
            IncrementLogCounter();

            if (orderRequest == null)
            {
                throw new ApiException(HttpStatusCode.BadRequest, FraudApiErrorResource.InvalidRequestCode, FraudApiErrorResource.InvalidRequestMessage);
            }
            
            if (String.IsNullOrWhiteSpace(orderRequest.MerchantId))
            {
                throw new ApiException(HttpStatusCode.BadRequest, FraudApiErrorResource.InvalidRequestCode, FraudApiErrorResource.InvalidMerchantIDMessage);
            }

            SetCredentials(orderRequest);

            if (String.IsNullOrWhiteSpace(orderRequest.Type))
            {
                throw new ApiException(HttpStatusCode.BadRequest, FraudApiErrorResource.InvalidRequestCode, FraudApiErrorResource.InvalidTypeMessage);
            }
            if (String.IsNullOrWhiteSpace(orderRequest.SubType))
            {
                throw new ApiException(HttpStatusCode.BadRequest, FraudApiErrorResource.InvalidRequestCode, FraudApiErrorResource.InvalidSubTypeMessage);
            }
            if (String.IsNullOrWhiteSpace(orderRequest.Username))
            {
                throw new ApiException(HttpStatusCode.BadRequest, FraudApiErrorResource.InvalidRequestCode, FraudApiErrorResource.UsernameRequiredMessage);
            }

            if (String.IsNullOrWhiteSpace(orderRequest.Password))
            {
                throw new ApiException(HttpStatusCode.BadRequest, FraudApiErrorResource.InvalidRequestCode, FraudApiErrorResource.PasswordRequiredMessage);
            }

            IReport report;
            try
            {
                report = await _paymentInstrumentProvider.GetOrderDetail(orderRequest);
            }
            catch (Exception ex)
            {
                IncrementLogCounterError();
                throw GetApiException(ex);
            }
            if (report == null)
            {
                throw new ApiException(HttpStatusCode.NotFound, FraudApiErrorResource.GeneralNotFoundCode,
                                       FraudApiErrorResource.GeneralNotFoundMessage);
            }
            HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, report);
            return response;
        }

        private void SetCredentials(OrderRequest orderRequest)
        {
            if (_orderRequestSettingsSection == null || _orderRequestSettingsSection.Settings == null)
            {
                throw new ApiException(HttpStatusCode.InternalServerError, FraudApiErrorResource.InternalServerErrorCode, FraudApiErrorResource.InternalServerErrorMessage);
            }

            var requestSetting = _orderRequestSettingsSection.Settings.GetOrderRequestSetting(orderRequest.MerchantId);
            if (requestSetting == null)
            {
                throw new ApiException(HttpStatusCode.InternalServerError, FraudApiErrorResource.InternalServerErrorCode, FraudApiErrorResource.InternalServerErrorMessage);
            }

            orderRequest.Username = requestSetting.PaymentInstrumentUsername;
            orderRequest.Password = requestSetting.PaymentInstrumentPassword;
            orderRequest.VersionNumber = requestSetting.PaymentInstrumentVersion;
        }

    }
}