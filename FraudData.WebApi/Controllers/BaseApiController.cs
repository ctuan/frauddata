﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http;
using System.Web.Mvc;

namespace FraudData.WebApi.Controllers
{
    public abstract class BaseApiController : ApiController
    {
        public string Role
        {
            get
            {
                IEnumerable<string> values;
                var role = Request.Headers.TryGetValues("x-mashery-oauth-user-role", out values) ? values.FirstOrDefault() : null;
                return role ?? string.Empty;
            }
        }

    }
}
