﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Web;
using Microsoft.Practices.EnterpriseLibrary.Logging;
using Microsoft.Practices.ObjectBuilder2;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using RiskAssessment.Provider.Models;
using Starbucks.FraudData.Provider.Common.Models;

namespace FraudData.WebApi.Controllers
{
    public class LogHelper
    {
        #region publics
        public static void LogPayPalRequest(string userId, ITransactionRequest request)
        {
            try
            {
                var properties = new Dictionary<string, string>();
                string payPalRiskString = SerializeObject(request);
                properties.Add("PayPal risk request dump", payPalRiskString);
                WriteLog(TraceEventType.Information, "PayPal - FraudCheckDataForPayPal - Request", "Data sent to payPal for risk assessment", properties);
            }
            catch (Exception ex)
            {
                Debug.WriteLine(string.Format("Unable to serialize the request for {0}", userId));
            }
        }

        public static void LogErrors(AggregateException aex)
        {
            try
            {
                aex.Handle(WriteException);
            }
            catch
            {
                Debug.Write("Unable to log errors.");
            }
        }

        public static bool WriteException(Exception ex)
        {
            try
            {
                var properties = new Dictionary<string, string>();
                properties.Add("Stacktrace", ex.StackTrace);
                WriteLog(TraceEventType.Error, "PayPal - Risk Error", ex.Message, properties);
                return true;
            }
            catch
            {
                return false;
            }

        }

        public static void LogTimer(Stopwatch stopWatch, string message, Dictionary<string, string> extendedProperties = null)
        {
            TimeSpan ts = stopWatch.Elapsed;
            string elapsedTime = String.Format("{0:00}:{1:00}:{2:00}.{3:000}",
                ts.Hours, ts.Minutes, ts.Seconds,
                ts.Milliseconds);
            WriteLog(TraceEventType.Information, "PayPal - Risk Timer", string.Format("{0}: elapsedTime: {1}", message, elapsedTime), extendedProperties);
        }

        public static void LogPayPalDetails(string userId, ITransactionResponse response)
        {
            try
            {
                var properties = new Dictionary<string, string>();
                properties.Add("UserId", userId);
                properties.Add("PaypalAccountId.PayerId", (response.senderAccount != null && response.senderAccount.PaypalAccountId != null) ? response.senderAccount.PaypalAccountId.PayerId : string.Empty);
                properties.Add("request.trackingId", response.TrackingId);
                properties.Add("responseEnvelope.AckCode", response.ResponseEnvelope.Ack.ToString());
                properties.Add("responseEnvelope.CorrelationId", response.ResponseEnvelope.CorrelationId);
                if (response.IpAddress != null)
                {
                    properties.Add("ipAddress.ipAddress", response.IpAddress.IPAddress);
                }
                if ((response.senderAccount != null && response.senderAccount.PartnerAccount != null))
                {
                    properties.Add("partnerAccount.email", response.senderAccount.PartnerAccount.email);
                    properties.Add("partnerAccount.firstName", response.senderAccount.PartnerAccount.FirstName);
                    properties.Add("partnerAccount.lastName", response.senderAccount.PartnerAccount.LastName);
                    properties.Add("partnerAccount.phone", response.senderAccount.PartnerAccount.Phone);
                    properties.Add("partnerAccount.createDate",
                                   response.senderAccount.PartnerAccount.CreateDate.ToString());
                    properties.Add("partnerAccount.lastGoodTransactionDate",
                                   response.senderAccount.PartnerAccount.LastGoodTransactionDate.ToString());
                    properties.Add("partnerAccount.transactionCountThreeMonths",
                                   response.senderAccount.PartnerAccount.TransactionCountThreeMonths.ToString());
                }

                response.AdditionalData.ForEach(p => properties.Add(string.Format("additionalData.{0}", p.Key), p.Value));
                if (response.SubOrders != null && response.SubOrders.LineItems != null)
                {
                    foreach (var item in response.SubOrders.LineItems.Select(lineItem => lineItem.RetailLineItem).OfType<RetailLineItem>())
                    {
                        properties.Add(string.Format("LineItem.Identifier - {0}", item.Identifier), item.Identifier);
                        properties.Add(string.Format("LineItem.ItemPrice - {0}", item.Identifier), item.ItemPrice.ToString());
                        properties.Add(string.Format("LineItem.CurrencyCode - {0}", item.Identifier), item.CurrencyCode);
                        properties.Add(string.Format("Line.Item.Name - {0}", item.Identifier), item.Name);
                    }
                }
                WriteLog(TraceEventType.Information, "PayPal - FraudCheckDataForPayPal - Response", "Response from payPal risk assessment", properties); // setting event type to error so that OM logs it.
            }
            catch (Exception ex)
            {
                Logger.Write(string.Format("Send Additional data to Paypal logging exception : {0}\n{1}", ex.Message, ex.StackTrace), "APILog", 1, 0, TraceEventType.Error, "FraudCheckDataForPayPal");
            }
        }
        #endregion

        #region privates

        private static string SerializeObject(object obj)
        {
            var result = JsonConvert.SerializeObject(obj, Formatting.Indented, GetSerializerSettings());
            return result;
        }

        private static JsonSerializerSettings GetSerializerSettings()
        {
            var settings = new JsonSerializerSettings()
            {
                ReferenceLoopHandling = ReferenceLoopHandling.Error
            };
            settings.Converters.Add(new StringEnumConverter());
            return settings;
        }

        private static void WriteLog(TraceEventType eventType, string title, string message, Dictionary<string, string> properties)
        {
            try
            {
                var log = new LogEntry { Message = message, EventId = 100, Title = title, Severity = eventType };
                log.Categories.Add("APILog");
                foreach (var item in properties)
                {
                    if (!log.ExtendedProperties.ContainsKey(item.Key))
                    {
                        log.ExtendedProperties.Add(item.Key, item.Value ?? string.Empty);
                    }
                }
                Logger.Write(log);
            }
            catch
            {
                // do nothing
            }
        }
        #endregion

    }
}