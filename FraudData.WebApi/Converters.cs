﻿using FraudData.WebApi.Models;
using System;
using System.Linq;
using Starbucks.FraudData.Provider.Common.Models;

namespace FraudData.WebApi
{
    public static class Converters
    {
        public static FraudCheckData ToApi(this IFraudCheckData fraudCheckData)
        {
            return new FraudCheckData
            {
                OrderSource = fraudCheckData.OrderSource,
                SvcNumber = fraudCheckData.SvcNumber,
                CardRegistrationDate = fraudCheckData.CardRegistrationDate,
                SvcNumberReloaded = fraudCheckData.SvcNumberReloaded,
                IsCardCurrentlyRegisteredToUser = fraudCheckData.IsCardCurrentlyRegisteredToUser,
                IsUserSignedIn = fraudCheckData.IsUserSignedIn,
                EmailAddress = fraudCheckData.EmailAddress,
                UserName = fraudCheckData.UserName,
                PaypalPayerId = fraudCheckData.PaypalPayerId,
                UserId = fraudCheckData.UserId,
                AgeOfAccountSinceCreation = fraudCheckData.AgeOfAccountSinceCreation,
                IsPartner = fraudCheckData.IsPartner,
                BirthDay = fraudCheckData.BirthDay,   // combine with birth month in MM\DD format
                BirthMonth = fraudCheckData.BirthMonth,
                MailSignUp = fraudCheckData.MailSignUp,
                eMailSignUp = fraudCheckData.eMailSignUp,
                TextMessageSignUp = fraudCheckData.TextMessageSignUp,
                TwitterSignUp = fraudCheckData.TwitterSignUp,
                FacebookSignUp = fraudCheckData.FacebookSignUp,
                PurchaserIpAddress = fraudCheckData.PurchaserIpAddress,
                Nickname = fraudCheckData.Nickname,
                IsDefaultSvcCard = fraudCheckData.IsDefaultSvcCard,
                TotalCardsRegistered = fraudCheckData.TotalCardsRegistered,
                IsAutoreload = fraudCheckData.IsAutoreload,
                BalancePriorToReload = fraudCheckData.BalancePriorToReload,
                RewardTierLevel = fraudCheckData.RewardTierLevel,
                DaysSinceLastReload = fraudCheckData.DaysSinceLastReload,
                TimeSinceLastPOSTransaction = fraudCheckData.TimeSinceLastPOSTransaction,
                PurchaserSessionDuration = fraudCheckData.PurchaserSessionDuration,
                MDDFields = fraudCheckData.MDDFields
            };
        }

        public static Starbucks.FraudData.Dal.Common.Models.IClientData ToProvider(this ClientData clientData)
        {
            return new Starbucks.FraudData.Dal.Sql.Models.ClientData
            {
                UserId = clientData.UserId,
                CardId = clientData.CardId,
                PaymentMethodNickName = clientData.PaymentMethodNickName,
                PurchaserIpAddress = clientData.PurchaserIpAddress,
                PurchaserSessionDuration = clientData.PurchaserSessionDuration,
                OrderSource = clientData.OrderSource,
                IsAutoReload = clientData.IsAutoReload,
                PaypalPayerId = clientData.PaypalPayerId
            };

        }

        public static SetTransactionContextResponse ToApiResponse(this ITransactionResponse response)
        {
            var transactionResponse = new SetTransactionContextResponse();
            transactionResponse.ResponseEnvelope = new ResponseEnvelope();
            transactionResponse.ResponseEnvelope.Ack = response.ResponseEnvelope.Ack.ToProviderAckType();
            transactionResponse.ResponseEnvelope.Build = response.ResponseEnvelope.Build;
            transactionResponse.ResponseEnvelope.CorrelationId = response.ResponseEnvelope.CorrelationId;
            transactionResponse.ResponseEnvelope.Timestamp = response.ResponseEnvelope.Timestamp;
            transactionResponse.TrackingId = response.TrackingId;
            if (response.IpAddress != null)
            {
                transactionResponse.IpAddress = new IpAddress { IPAddress = response.IpAddress.IPAddress };
            }

            if (response.AdditionalData != null)
            {
                transactionResponse.AdditionalData = new Pair[2];
                transactionResponse.AdditionalData = (from data in response.AdditionalData
                                                      select new Pair
                                                      {
                                                          Key = data.Key,
                                                          Value = data.Value
                                                      }).ToArray();
                
            }
            if (response.senderAccount != null)
            {
                transactionResponse.SenderAccount = new Models.Account();
                if (response.senderAccount.PaypalAccountId != null)
                {
                    transactionResponse.SenderAccount.PaypalAccountID = new PaypalAccountIdentifier
                    {
                        PayerId = response.senderAccount.PaypalAccountId.PayerId
                    };
                }

                if (response.senderAccount.PartnerAccount != null)
                {
                    transactionResponse.SenderAccount.PartnerAccount = new PartnerAccount
                    {
                        email = response.senderAccount.PartnerAccount.email,
                        FirstName = response.senderAccount.PartnerAccount.FirstName,
                        LastName = response.senderAccount.PartnerAccount.LastName,
                        Phone = response.senderAccount.PartnerAccount.Phone,
                        LastGoodTransactionDate = response.senderAccount.PartnerAccount.LastGoodTransactionDate,
                        CreateDate = response.senderAccount.PartnerAccount.CreateDate,
                        TransactionCountThreeMonths = response.senderAccount.PartnerAccount.TransactionCountThreeMonths
                    };
                }
            }

            transactionResponse.SubOrder = ConvertToApiSubOrder(response.SubOrders);
            return transactionResponse;
        }
        public static SubOrder ConvertToApiSubOrder(ISubOrder order)
        {
            if (order == null || order.LineItems == null)
            {
                return null;
            }
            var requestOrder = new SubOrder { LineItems = new LineItem[order.LineItems.Length] };
            requestOrder.LineItems = order.LineItems.ToList().ConvertAll(ConvertToApiLineItem).ToArray();
            return requestOrder;
        }

        public static LineItem ConvertToApiLineItem(ILineItem input)
        {
            var retail = new LineItem
                {
                    RetailLineItem =
                      new RetailLineItem
                      {
                          CurrencyCode = input.RetailLineItem.CurrencyCode,
                          Identifier = input.RetailLineItem.Identifier,
                          ItemCount = input.RetailLineItem.ItemCount,
                          ItemPrice = input.RetailLineItem.ItemPrice,
                          Name = input.RetailLineItem.Name
                      }
                };

            return retail;
        }
        public static Models.AckCode ToProviderAckType(this Starbucks.FraudData.Provider.Common.Models.AckCode ack)
        {
            Models.AckCode ackApi;
            Enum.TryParse(ack.ToString(), true, out ackApi);
            return ackApi;
        }

        public static RiskAssessment.Provider.Models.SubOrder ConvertToProviderSubOrder(SubOrder order)
        {
            var requestOrder = new RiskAssessment.Provider.Models.SubOrder { LineItems = new ILineItem[order.LineItems.Length] };
            requestOrder.LineItems = order.LineItems.ToList().ConvertAll(ConvertToProviderLineItem).ToArray();
            return requestOrder;
        }

        public static ILineItem ConvertToProviderLineItem(LineItem input)
        {
            var retail = new RiskAssessment.Provider.Models.LineItem
            {
                RetailLineItem = 
                    new RiskAssessment.Provider.Models.RetailLineItem
                    {
                        CurrencyCode = input.RetailLineItem.CurrencyCode,
                        Identifier = input.RetailLineItem.Identifier,
                        ItemCount = input.RetailLineItem.ItemCount,
                        ItemPrice = input.RetailLineItem.ItemPrice,
                        Name = input.RetailLineItem.Name
                    }
            };
            return retail;
        }
    }
}