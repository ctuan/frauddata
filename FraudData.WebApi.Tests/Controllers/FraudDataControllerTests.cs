﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using FraudData.WebApi.Controllers;
using Starbucks.TestTools.Unit.Mvc;
using Starbucks.OpenApi.ServiceExtensions.Exceptions;
using Starbucks.FraudData.WebApi.Models;
using Starbucks.FraudData.Provider.Common;
using System.Threading.Tasks;
using FraudData.WebApi.Tests.Models;
using Starbucks.FraudData.Proxy.Common.Models;
using System.Net.Http;
using System.Net;
using FraudData.WebApi.Models;
using Starbucks.FraudData.Provider.Common.Models;
using Moq;

namespace FraudData.WebApi.Tests.Controllers
{
    [TestClass]
    public class FraudDataControllerTests : TestForApiController<FraudDataController>
    {
        private ClientData _clientData;
        private FraudCheckData _fraudCheckData;
        private HttpResponseMessage _response;
        private static readonly Guid ExistingUserId = new Guid("2127F74D-1852-4702-8831-196D214DD9AA");
        private static readonly string CardId = "856F73F09CD21C";
        private Guid _userId;

        [TestInitialize]
        public void Initialize()
        {
            _clientData = new ClientData()
            {
                UserId = ExistingUserId.ToString(),
                CardId = CardId,
                IsAutoReload = false,
                OrderSource = "starbucks.com",
                PaymentMethodNickName = "My Visa",
                PaypalPayerId = "",
                PurchaserIpAddress = "1.2.3.4",
                PurchaserSessionDuration = 20.43M
            };
        }
   
        [TestMethod]
        [ExpectedException(typeof(ApiException))]
        public void GetUserData_UserId_Null_Request_Should_Throw()
        {
            _clientData.UserId = null;           
            WhenGetUserDataForCyberSourceCalled();
          
        }

        [TestMethod]
        public void GetUserData_Valid_Request_Should_Return_All()
        {
            GivenClientData();
            WhenGetUserDataForCyberSourceCalled();
            ThenResponseStatusShouldBeOk();
            ThenTheResponseShouldHaveTheUserData();

        }

        private void GivenClientData()
        {
            IFraudCheckData response = new Starbucks.FraudData.Provider.Models.FraudCheckData();
            response.RewardTierLevel = "Tier 1";
            response.UserId = ExistingUserId.ToString();
            response.TimeSinceLastPOSTransaction = 20;
            MockOf<ICyberSourceFraudDataProvider>().Setup(p => p.GetUserDataForFraudCheck(It.IsAny<Starbucks.FraudData.Dal.Common.Models.IClientData>())).Returns(response);
        }

        public void WhenGetUserDataForCyberSourceCalled()
        {
            _response = Target.GetUserDataForCyberSource(_clientData);
        }

        private void ThenResponseStatusShouldBeOk()
        {
            Assert.AreEqual(HttpStatusCode.OK, _response.StatusCode);
        }

        private void ThenTheResponseShouldHaveTheUserData()
        {
            WebApi.Models.FraudCheckData fraudData;
            var succeed = _response.TryGetContentValue<WebApi.Models.FraudCheckData>(out fraudData);
            Assert.IsTrue(succeed);
            Assert.IsNotNull(fraudData);
            Assert.AreEqual(_clientData.UserId, fraudData.UserId);
        }
    }
}
