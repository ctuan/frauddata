﻿using System;
using FraudData.WebApi.Configuration;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using FraudData.WebApi.Controllers;
using Newtonsoft.Json.Bson;
using Starbucks.TestTools.Unit.Mvc;
using Starbucks.OpenApi.ServiceExtensions.Exceptions;
using Starbucks.FraudData.WebApi.Models;
using Starbucks.FraudData.Provider.Common;
using System.Threading.Tasks;
using FraudData.WebApi.Tests.Models;
using Starbucks.FraudData.Proxy.Common.Models;
using System.Net.Http;
using System.Net;

namespace FraudData.WebApi.Tests.Controllers
{
    [TestClass]
    public class ReportControllerTests : TestForApiController<OrderReportController>
    {
        private OrderRequest _orderRequest;
        private HttpResponseMessage _response;

        [TestInitialize]
        public void Initialize()
        {
            _orderRequest = new OrderRequest
            {
                MerchantId = "starbucksreload",
                MerchantReferenceNumber = "OrderID",
                SubType = "transactionDetail",
                Type = "transaction"
            };
        }

        [TestMethod]
        public void GetOrderDetail_Null_Request_Should_Throw()
        {
            _orderRequest = null;

            try
            {
                WhenGetOrderDetailIsCalled();
            }
            catch (AggregateException ex)
            {
                Assert.IsTrue(ex.InnerException.GetType() == typeof(ApiException));
            }
        }

        [TestMethod]
        public void GetOrderDetail_MerchantId_Null_Request_Should_Throw()
        {
            _orderRequest.MerchantId = null;
            GivenOrderRequestSettingsSection();

            try
            {
                WhenGetOrderDetailIsCalled();
            }
            catch (AggregateException ex)
            {
                Assert.IsTrue(ex.InnerException.GetType() == typeof(ApiException));
            }
        }

        [TestMethod]
        public void GetOrderDetail_Type_Null_Request_Should_Throw()
        {
            _orderRequest.Type = null;
            GivenOrderRequestSettingsSection();
            try
            {
                WhenGetOrderDetailIsCalled();
            }
            catch (AggregateException ex)
            {
                Assert.IsTrue(ex.InnerException.GetType() == typeof(ApiException));
            }
        }

        [TestMethod]
        public void GetOrderDetail_SubType_Null_Request_Should_Throw()
        {
            _orderRequest.SubType = null;
            GivenOrderRequestSettingsSection();
            try
            {
                WhenGetOrderDetailIsCalled();
            }
            catch (AggregateException ex)
            {
                Assert.IsTrue(ex.InnerException.GetType() == typeof(ApiException));
            }
        }

        [TestMethod]
        public void GetOrderDetail_Username_Null_Request_Should_Throw()
        {
            _orderRequest.Username = null;
            GivenOrderRequestSettingsSection();
            try
            {
                WhenGetOrderDetailIsCalled();
            }
            catch (AggregateException ex)
            {
                Assert.IsTrue(ex.InnerException.GetType() == typeof(ApiException));
            }
        }

        [TestMethod]
        public void GetOrderDetail_Password_Null_Request_Should_Throw()
        {
            _orderRequest.Password = null;
            GivenOrderRequestSettingsSection();
            try
            {
                WhenGetOrderDetailIsCalled();
            }
            catch (AggregateException ex)
            {
                Assert.IsTrue(ex.InnerException.GetType() == typeof(ApiException));
            }
        }

        [TestMethod]
        public void GetOrderDetail_Valid_Request_Should_Return_Report()
        {
            GivenPaymentInstrument();
            GivenOrderRequestSettingsSection();
            WhenGetOrderDetailIsCalled();
            ThenResponseStatusShouldBeOk();
            ThenTheResponseShouldHaveTheReport();

        }

        private void ThenTheResponseShouldHaveTheReport()
        {
            IReport report;
            var succeed = _response.TryGetContentValue<IReport>(out report);
            Assert.IsTrue(succeed);
            Assert.IsNotNull(report);
            Assert.AreEqual(_orderRequest.MerchantId, report.MerchantID);
        }

        private void ThenResponseStatusShouldBeOk()
        {
            Assert.AreEqual(HttpStatusCode.OK, _response.StatusCode);
        }

        private void WhenGetOrderDetailIsCalled()
        {

            var response = Target.GetOrderDetail(_orderRequest);
            _response = response.Result;
        }

        private void GivenOrderRequestSettingsSection()
        {
            MockOf<IOrderRequestSettingsSection>().SetupGet(section => section.Settings).Returns(
                 new OrderRequestSettingCollection
                {
                    new OrderRequestSettingElement
                    {
                        MerchantKey = "Starbucksreload",
                        PaymentInstrumentUsername = "chala",
                        PaymentInstrumentPassword = "chubeChebete"
                    }
                }
            );
        }

        private void GivenPaymentInstrument()
        {
            IReport report = new Report
                    {
                        MerchantID = _orderRequest.MerchantId,
                        Name = "StarbucksReload",
                        Version = "1.3"
                    };
            MockOf<IPaymentInstrumentServiceProvider>().Setup(p => p.GetOrderDetail(_orderRequest)).Returns(Task.FromResult(report));

        }
    }
}
