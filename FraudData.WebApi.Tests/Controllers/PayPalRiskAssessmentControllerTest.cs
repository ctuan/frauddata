﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Web.Http;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using FraudData.WebApi;
using Starbucks.TestTools.Unit.Mvc;
using FraudData.WebApi.Controllers;
using FraudData.WebApi.Models;
using System.Threading.Tasks;
using Starbucks.FraudData.Provider.Common;
using Starbucks.FraudData.Provider.Common.Models;
using Moq;
//using RiskAssessment.Provider.Models;

namespace FraudData.WebApi.Tests.Controllers
{
    

    [TestClass]
    public class PayPalRiskAssessmentControllerTest : TestForApiController<PayPalRiskAssessmentController>
    {
        private static readonly Guid ExistingUserId = new Guid("4E78DE32-42FE-4D0A-B805-C64FEC280804");
        private Guid _userId;
        private const string Culture = "en-US";
        SetTransactionContextRequest _transactionRequest;

        private dynamic _response;
        private Task<ITransactionResponse> _expectedTransactionresponse;

        [TestInitialize]
        public void TestInitialize()
        {   
            _expectedTransactionresponse = TaskHelpers.CreatePseudoTask<ITransactionResponse>(DoAction()); 
           
        }

        private RiskAssessment.Provider.Models.TransactionResponse DoAction()
        {
            var response = new RiskAssessment.Provider.Models.TransactionResponse();
            response.ResponseEnvelope = new RiskAssessment.Provider.Models.ResponseEnvelope();
            response.TrackingId = "EC-9X436600J4122373M";
            response.ResponseEnvelope.Ack = Starbucks.FraudData.Provider.Common.Models.AckCode.Success;           
            return response;
        }
        
        [TestMethod]
        public async Task Given_TrackingIdSetTransactionContext()
        {
            // Arrange:
            Given_UserId(ExistingUserId);
            Given_TransactionRequest();
            GivenFraudDataProvider();

            // Act:
            await When_SetTransactionContext();
          
            // Assert:
            Then_EnvelopeResponseShoudReturn();
        }

        private void Given_UserId(Guid existingUserId)
        {
            _userId = existingUserId;
        }

         private void Given_TransactionRequest()
        {
            _transactionRequest = new SetTransactionContextRequest();
            _transactionRequest.RequestEnvelope = new RequestEnvelope();
            _transactionRequest.RequestEnvelope.ErrorLanguage = "en-US";
            _transactionRequest.TrackingId = "EC-9X436600J4122373M";        

            _transactionRequest.IpAddress = new IpAddress();
            _transactionRequest.IpAddress.IPAddress = "10.6.33.106";

            _transactionRequest.SubOrders = new SubOrder[1];
            var requestOrder = new SubOrder();
            requestOrder.LineItems = new LineItem[1];
            var retail = new LineItem();
            retail.RetailLineItem = new RetailLineItem();
            retail.RetailLineItem.CurrencyCode = "US";
            retail.RetailLineItem.Identifier = "1";
            retail.RetailLineItem.ItemPrice = 0.99M;
            retail.RetailLineItem.Name = "some name";
            requestOrder.LineItems[0] = retail;

            _transactionRequest.SubOrders[0] = requestOrder;  
        }

         private async Task When_SetTransactionContext()
         {                  
             _response = await Target.SetTransactionContext(ExistingUserId.ToString(), _transactionRequest);
            
         }

         private void Then_EnvelopeResponseShoudReturn()
         {

             if (_response.IsSuccessStatusCode)
             {
                 Assert.IsNotNull(_response);
             }
             else
             {
                 Assert.Fail();
             }
         }
        
         private void GivenFraudDataProvider()
         {
             MockOf<IPayPalFraudDataProvider>().Setup(pr => pr.TransactionContext(It.IsAny<string>(), It.IsAny<ITransactionRequest>())).Returns(_expectedTransactionresponse);              
         }

         
    }

    internal class TaskHelpers
    {
        public static Task<T> CreatePseudoTask<T>(T result)
        {
            var tcs = new TaskCompletionSource<T>();
            tcs.SetResult(result);
            return tcs.Task;
        }
    }
}
