﻿using Starbucks.FraudData.Proxy.Common.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FraudData.WebApi.Tests.Models
{
    public class Report : IReport
    {
        public string MerchantID { get; set; }
        
        public string Name { get; set; }

        public string ReportEndDate { get; set; }

        public string ReportStartDate { get; set; }

        public IEnumerable<IRequest> Requests { get; set; }

        public string Version { get; set; }
    }
}
