﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Starbucks.FraudData.Dal.Common.Models
{
    public interface IPayPalSenderAccount
    {
        string PayerId { get; set; }

        string EmailAddress { get; set; }

        string  FirstName { get; set; }

        string LastName { get; set; }

        DateTime? CreateDate { get; set; }

        string PhoneNumber { get; set; }

        decimal AverageTxnAmount { get; set; }

        double TxnFrequencyInDays { get; set; }

        DateTime LastGoodTransactionDate { get; set; }

        int TransactionCountThreeMonths { get; set; }
    }
}
