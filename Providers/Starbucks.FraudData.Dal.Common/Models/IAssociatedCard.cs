﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Starbucks.FraudData.Dal.Common.Models
{
    public interface IAssociatedCard
    {
        string SubMarketCode { get; set; }
        bool IsDefault { get; set; }
        bool IsOwner { get; set; }
        string Nickname { get; set; }
        string RegisteredUserId { get; set; }
        DateTime? RegistrationDate { get; set; }
        string RegistrationSource { get; set; }
        string Number { get; set; }
        decimal? Balance { get; set; }
    }
}
