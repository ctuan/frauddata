﻿using Starbucks.FraudData.Dal.Common.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Starbucks.FraudData.Dal.Common
{
    public interface ICyberSourceFraudDataDal
    {
        IFraudCheckData GetUserDataForFraudCheck(IClientData clientData, IFraudCheckData result);
    }
}
