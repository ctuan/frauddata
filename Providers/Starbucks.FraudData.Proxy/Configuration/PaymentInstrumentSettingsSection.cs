﻿using System.Configuration;

namespace Starbucks.FraudData.Proxy.Configuration
{
    public class PaymentInstrumentSettingsSection : ConfigurationSection
    {
        [ConfigurationProperty("paymentInstrumentUrl")]
        public string PaymentInstrumentUrl
        {
            get { return this["paymentInstrumentUrl"] as string; }
            set { this["paymentInstrumentUrl"] = value; }
        }       
    }
}
