﻿using Starbucks.FraudData.Proxy.Common;
using Starbucks.FraudData.Proxy.Configuration;
using System;
using System.Configuration;
using System.IO;
using System.Net;
using System.Text;
using System.Threading.Tasks;

namespace Starbucks.FraudData.Proxy
{
    public class PaymentInstrumentServiceProxy : IPaymentInstrumentServiceProxy
    {
        private PaymentInstrumentSettingsSection _paymentInstrumentSettingsSection;

        public PaymentInstrumentServiceProxy(PaymentInstrumentSettingsSection paymentInstrumentSettingsSection)
        {
            if (paymentInstrumentSettingsSection == null)
            {
                throw new ArgumentNullException("cannot be null", "paymentInstrumentSettingsSection");
            }
            if (String.IsNullOrWhiteSpace(paymentInstrumentSettingsSection.PaymentInstrumentUrl))
            {
                throw new ArgumentException("cannot be null or empty", "paymentInstrumentSettingsSection.PaymentInstrumentUrl");
            }

            _paymentInstrumentSettingsSection = paymentInstrumentSettingsSection;
        }

        public async Task<string> GetOrderDetailAsync(NetworkCredential credential, string postdata)
        {
            if (credential == null)
            {
                throw new ArgumentNullException("cannot be null", "credential");
            }
            if (String.IsNullOrWhiteSpace(postdata))
            {
                throw new ArgumentNullException("cannot be null or empty", "postData");
            }

            var textReader = await DoRequestAsync(credential, postdata);
            var response = await textReader.ReadToEndAsync();
            return response;
        }

        private async Task<TextReader> DoRequestAsync(NetworkCredential credential, string postData)
        {
            HttpWebRequest request = HttpWebRequest.CreateHttp(_paymentInstrumentSettingsSection.PaymentInstrumentUrl);
            request.Method = WebRequestMethods.Http.Post;
            request.Credentials = credential;
            request.ContentType = "application/x-www-form-urlencoded";
            var textReader = await DoRequestAsync(request, postData);
            return textReader;
        }

        private async Task<TextReader> DoRequestAsync(WebRequest request, string postData)
        {
            byte[] bytes = Encoding.UTF8.GetBytes(postData);
            request.ContentLength = bytes.Length;

            using (var writeStream = request.GetRequestStream())
            {
                writeStream.Write(bytes, 0, bytes.Length);
            }

            var task = Task.Factory.FromAsync((cb, o) => ((HttpWebRequest)o).BeginGetResponse(cb, o)
                                  , res => ((HttpWebRequest)res.AsyncState).EndGetResponse(res)
                                  , request);
            var result = await task;
            var stream = result.GetResponseStream();
            var sr = new StreamReader(stream);
            return sr;
        }
    }

}
