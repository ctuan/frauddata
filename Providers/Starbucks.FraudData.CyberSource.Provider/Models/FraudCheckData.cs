﻿using Starbucks.FraudData.Provider.Common.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Starbucks.FraudData.CyberSource.Provider.Models
{
    public class FraudCheckData : IFraudCheckData
    {
        public string OrderSource { get; set; }

        public string SvcNumber { get; set; }

        public DateTime? CardRegistrationDate { get; set; }

        public string SvcNumberReloaded { get; set; }

        public string CallCenterRepId { get; set; }

        public bool IsCardCurrentlyRegisteredToUser { get; set; }

        public string ShippingMethod { get; set; }

        public bool IsUserSignedIn { get; set; }

        public string UserName { get; set; }

        public string EmailAddress { get; set; }

        public string PaypalPayerId { get; set; }

        public string UserId { get; set; }

        public int AgeOfAccountSinceCreation { get; set; }

        public bool IsPartner { get; set; }

        public int BirthDay { get; set; }

        public int BirthMonth { get; set; }

        public bool eMailSignUp { get; set; }

        public bool MailSignUp { get; set; }

        public bool TextMessageSignUp { get; set; }

        public bool TwitterSignUp { get; set; }

        public bool FacebookSignUp { get; set; }

        public decimal PurchaserSessionDuration { get; set; }

        public string PurchaserIpAddress { get; set; }

        public decimal CardBalance { get; set; }

        public string Currency { get; set; }

        public string Nickname { get; set; }

        public bool IsDefaultSvcCard { get; set; }

        public int TotalCardsRegistered { get; set; }

        public string SessionId { get; set; }

        public bool IsAutoreload { get; set; }

        public decimal BalancePriorToReload { get; set; }

        public string RewardTierLevel { get; set; }

        public int? DaysSinceLastReload { get; set; }

        public int? TimeSinceLastPOSTransaction { get; set; }

        public Dal.Common.Models.IMDDField[] MDDFields { get; set; }
    }
}
