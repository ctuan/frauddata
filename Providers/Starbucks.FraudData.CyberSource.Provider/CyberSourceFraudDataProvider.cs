﻿using Starbucks.FraudData.Provider.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Starbucks.FraudData.Dal.Common;
using Starbucks.FraudData.Dal.Sql.Models;
using Starbucks.Rewards.Provider.Common;
using Starbucks.TransactionHistory.Dal.Common;
using Starbucks.Rewards.Common.Models;
using Starbucks.TransactionHistory.Common.Models.Response;
using Starbucks.TransactionHistory.Common.Enums;
using System.Configuration;
using Starbucks.TransactionHistory.Common.Models;
using ImpromptuInterface;
using Starbucks.FraudData.Provider.Common.Models;
using Microsoft.Practices.EnterpriseLibrary.Logging;


namespace Starbucks.FraudData.CyberSource.Provider
{
    public class CyberSourceFraudDataProvider : ICyberSourceFraudDataProvider
    {
        private readonly ICyberSourceFraudDataDal _fraudDataDal;
        private readonly IRewardsProvider _rewardsProvider;
        private readonly ITransactionHistoryDataProvider _transactionHistoryDataProvider;

        public CyberSourceFraudDataProvider(ICyberSourceFraudDataDal fraudDataDal, IRewardsProvider rewardsProvider, ITransactionHistoryDataProvider transactionHistoryDataProvider)
        {
            _fraudDataDal = fraudDataDal;
            _rewardsProvider = rewardsProvider;
            _transactionHistoryDataProvider = transactionHistoryDataProvider;
        }

        /// <summary>
        ///  fetches user data to pass to cybersource for fraud check
        /// </summary>
        /// <param name="userId"></param>
        /// <param name="cardId"></param>
        /// <returns></returns>
        public IFraudCheckData GetUserDataForFraudCheck(Dal.Common.Models.IClientData clientData)
        {
            Dal.Common.Models.IFraudCheckData fraudCheckData = new FraudCheckData();
            if (clientData == null)
                return fraudCheckData.ToApi();
            try
            {
                fraudCheckData.MDDFields = InitializeArray<MDDField>(17);
                Parallel.Invoke(() =>
                {
                    _fraudDataDal.GetUserDataForFraudCheck(clientData, fraudCheckData);
                },
                () =>
                {
                    GetRewardsDataForFraudCheck(clientData.UserId, fraudCheckData);
                },
                () =>
                {
                    GetTransactionHistoryDataForFraudCheck(clientData.UserId, fraudCheckData);
                }
                );
            }
            catch (Exception ex)
            {
                Logger.Write(String.Format("Error while trying to fetch user data for fraud check in method {0}.{1}\n{2}. User id is {3}. Paypal Payer id is {4}", "GetUserDataForFraudCheck",
                                           ex.Message, ex.StackTrace, clientData.UserId, clientData.PaypalPayerId ?? ""));
            }
            return fraudCheckData.ToApi();
        }

        /// <summary>
        /// fetches user rewards data
        /// </summary>
        /// <param name="userId"></param>
        /// <param name="fraudCheckData"></param>
        private void GetRewardsDataForFraudCheck(string userId, Dal.Common.Models.IFraudCheckData fraudCheckData)
        {
            try
            {
                IRewardsSummary rewardsSummary = _rewardsProvider.GetRewardsSummary(userId, null);
                if (rewardsSummary != null)
                {
                    var tierInfo = rewardsSummary.MyTiers.FirstOrDefault(c => c.TierLevelName == rewardsSummary.CurrentLevel);
                    fraudCheckData.RewardTierLevel = string.Format("Tier {0}", tierInfo.TierNumber);
                }
                else
                    fraudCheckData.RewardTierLevel = string.Empty;
            }
            catch (Exception ex)
            {
                Logger.Write(String.Format("Error while trying to fetch rewards summary in method {0}.{1}\n{2}. User id is {3}", "GetRewardsDataForFraudCheck",
                                           ex.Message, ex.StackTrace, userId));
            }
        }

        /// <summary>
        /// fetches user transaction history data for the last 45 days
        /// </summary>
        /// <param name="userId"></param>
        /// <param name="fraudCheckData"></param>
        private void GetTransactionHistoryDataForFraudCheck(string userId, Dal.Common.Models.IFraudCheckData fraudCheckData)
        {
            var pageRequest = new { Offset = 0, Limit = int.MaxValue };
            var IpageRequest = pageRequest.ActLike<IPagedRequest>();
            int modifiedSince;
            int.TryParse(ConfigurationManager.AppSettings["TransactionHistoryPeriodToCybersource"], out modifiedSince);
            try
            {
                IPagedEnumerable<IHistoryItem> transactionHistory = _transactionHistoryDataProvider.GetHistoryItemsByModifiedDate(userId, null, DateTime.Now.AddDays(-modifiedSince), null, IpageRequest);
                var lastPOSTransaction = (from n in transactionHistory
                                          where n.TransactionDetails != null && n.TransactionDetails.StoreType == "Physical"
                                          orderby n.TransactionDetails.LocalTime descending
                                          select n).FirstOrDefault();

                var lastReloadTransaction = (from n in transactionHistory
                                             where n.TransactionDetails != null && n.TransactionDetails.TransactionType == TransactionType.Reload
                                             orderby n.TransactionDetails.LocalTime descending
                                             select n).FirstOrDefault();

                var rewardCoupon = transactionHistory.FirstOrDefault(p => p.RewardsCoupon != null &&
                                                                     p.RewardsCoupon.Status != TransactionHistory.Common.Enums.CouponStatus.Expired &&
                                                                     p.RewardsCoupon.LastRedemptionDate != null);

                var itemType = (from n in transactionHistory
                                where n.TransactionDetails != null
                                orderby n.TransactionDetails.LocalTime descending
                                select n).FirstOrDefault();

                var tipStatus = (from n in transactionHistory
                                 where n.TransactionDetails != null && n.TransactionDetails.TipInfo != null && n.TransactionDetails.TipInfo.TipTransactionId > 0
                                 orderby n.TransactionDetails.LocalTime descending
                                 select n.TransactionDetails.TipInfo).FirstOrDefault();

                if (lastPOSTransaction != null && lastPOSTransaction.TransactionDetails != null)
                    fraudCheckData.TimeSinceLastPOSTransaction = (int)(DateTime.Now - lastPOSTransaction.TransactionDetails.LocalTime).TotalDays;

                if (lastReloadTransaction != null && lastReloadTransaction.TransactionDetails != null)
                    fraudCheckData.DaysSinceLastReload = (int)(DateTime.Now - lastReloadTransaction.TransactionDetails.LocalTime).TotalDays;

                fraudCheckData.MDDFields[14] = new Starbucks.FraudData.Dal.Sql.Models.MDDField { ID = "35", Value = (rewardCoupon != null && rewardCoupon.RewardsCoupon != null && rewardCoupon.RewardsCoupon.CouponCode != null).ToString() };
                fraudCheckData.MDDFields[15] = new Starbucks.FraudData.Dal.Sql.Models.MDDField { ID = "36", Value = itemType != null ? itemType.ItemType.ToString() : "" };
                fraudCheckData.MDDFields[16] = new Starbucks.FraudData.Dal.Sql.Models.MDDField { ID = "37", Value = (tipStatus != null && tipStatus.IsTippable).ToString() };
            }
            catch (Exception ex)
            {
                Logger.Write(String.Format("Error while trying to fetch transaction history in method {0}.{1}\n{2}. User id is {3}", "GetTransactionHistoryDataForFraudCheck",
                                           ex.Message, ex.StackTrace, userId));
            }
        }

        #region Private Methods

        T[] InitializeArray<T>(int length) where T : MDDField, new()
        {
            T[] array = new T[length];
            for (int i = 0; i < length; ++i)
            {
                array[i] = new T() { ID = (21 + i).ToString() };
            }

            return array;
        }
        #endregion
    }

}
