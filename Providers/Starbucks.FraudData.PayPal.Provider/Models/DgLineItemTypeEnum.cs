﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;

namespace RiskAssessment.Provider.Models
{
    public enum DgLineItemTypeEnum
    {
        APPLICATION,
        IN_APPLICATION_PURCHASE,
        PLATFORM_CURRENCY,
        MUSIC,
        VIDEO,
        LITERATURE,
        OTHER
    }
}
