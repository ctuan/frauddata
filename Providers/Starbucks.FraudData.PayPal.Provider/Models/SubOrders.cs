﻿using Starbucks.FraudData.Provider.Common.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;

namespace RiskAssessment.Provider.Models
{
    public class SubOrder : ISubOrder
    {

        public ILineItem[] LineItems { get; set; }

        public IShippingInfo ShippingInfo { get; set; }

        public DeliveryEnum Delivery{ get; set; }
    }
}
