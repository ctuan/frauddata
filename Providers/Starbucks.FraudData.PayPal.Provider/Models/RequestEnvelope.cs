﻿using Starbucks.FraudData.Provider.Common.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;

namespace RiskAssessment.Provider.Models
{
    public class RequestEnvelope : IRequestEnvelope
    {
        public string ErrorLanguage { get; set; }

        public object Other { get; set; }
    }
}
