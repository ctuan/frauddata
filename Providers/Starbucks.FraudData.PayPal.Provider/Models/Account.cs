﻿using Starbucks.FraudData.Provider.Common.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;

namespace RiskAssessment.Provider.Models
{
    public class Account:IAccount
    {
        /// <summary>
        ///  Unique identifier for a Paypal account
        /// </summary>
        public IPaypalAccountIdentifier PaypalAccountId { get; set; }

        /// <summary>
        /// A Partner's view of the user with this paypalAccountID
        /// </summary>
        public IPartnerAccount PartnerAccount { get; set; }
    }
}
