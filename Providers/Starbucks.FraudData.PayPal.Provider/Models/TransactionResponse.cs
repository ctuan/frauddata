﻿using Starbucks.FraudData.Provider.Common.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Web;

namespace RiskAssessment.Provider.Models
{
    public class TransactionResponse : ITransactionResponse
    {
        /// <summary>
        ///  Application level acknowledgement code.
        /// </summary>
         public IResponseEnvelope ResponseEnvelope { get; set; }

        /// <summary>
        ///   A unique ID that you specify to track the payment. NOTE: You are responsible for ensuring that the ID is unique.
        /// </summary>
        public string TrackingId { get; set; }

        /// <summary>
        ///  Contains information about the sender's account
        /// </summary>
        public IAccount senderAccount { get; set; }

        /// <summary>
        /// Contains information about the receiver's account. When the API caller is the receiver, this field is not needed.
        /// </summary>
        public IAccount receiverAccount { get; set; }

        /// <summary>
        ///  Order information for this transaction.
        /// </summary>
        public ISubOrder SubOrders { get; set; }

        /// <summary>
        ///  Device data.
        /// </summary>
        public IDevice Device { get; set; }

        /// <summary>
        /// IP address for this transaction.
        /// </summary>
        public IIpAddress IpAddress { get; set; }

        /// <summary>
        /// A list of merchant specific data for the transaction.
        /// </summary>
        public IPair[] AdditionalData { get; set; }
    }
}