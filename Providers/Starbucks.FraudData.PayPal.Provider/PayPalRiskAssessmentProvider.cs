﻿using System;
using System.Linq;
using System.Threading.Tasks;
using RiskAssessment.TransactionContextProxy;
using System.Configuration;
using System.Net;
using System.ServiceModel;
using System.ServiceModel.Channels;
using Starbucks.FraudData.Provider;
using Starbucks.FraudData.Provider.Common;
using Starbucks.FraudData.Provider.Common.ErrorResources;
using Starbucks.FraudData.Provider.Common.Exceptions;
using commonModel = Starbucks.FraudData.Provider.Common.Models;
using Starbucks.FraudData.Dal.Common;

namespace RiskAssessment.Provider
{
    public class PayPalRiskAssessmentProvider : IPayPalFraudDataProvider
    {
        private readonly IPayPalFraudCheckData _paypalDataProvider;
        private readonly FraudDataProviderSettings _settings;
        public PayPalRiskAssessmentProvider(IPayPalFraudCheckData paypalDataProvider, FraudDataProviderSettings settings)
        {
            _paypalDataProvider = paypalDataProvider;
            _settings = settings;
        }

        public async Task<commonModel.ITransactionResponse> TransactionContext(string userId, commonModel.ITransactionRequest transactionRequest)
        {
            if (string.IsNullOrEmpty(transactionRequest.TrackingId))
            {
                throw new FraudDataProviderException(FraudDataProviderErrors.TrackingIdCannotBeNullCode, FraudDataProviderErrors.TrackingIdCannotBeNullMessage);
            }
            var client = new RiskAssessmentPortTypeClient();
            var request = CreateTransactionRequest(transactionRequest, userId);
            SetTransactionContextResponse response;
            try
            {
                response = await SetTransactionContext(client, request);
                client.Close();
            }
            catch (Exception ex)
            {
                client.Abort();
                if (ex is FaultException)
                {
                    var fault = (FaultException<FaultMessage>)ex;
                    if (fault.Message == FraudDataProviderErrors.UnableToGetRiskAssessmentScoresMessage)
                    {
                        throw new FraudDataProviderException(FraudDataProviderErrors.UnableToGetRiskAssessmentScoresCode, FraudDataProviderErrors.UnableToGetRiskAssessmentScoresMessage);
                    }
                }
                throw;
            }
            return ConversionExtensions.ToProvider(response);
        }

        private Task<SetTransactionContextResponse> SetTransactionContext(RiskAssessmentPortTypeClient client, SetTransactionContextRequest request)
        {
            //ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12;
            using (new OperationContextScope(client.InnerChannel))
            {
                var requestMessage = new HttpRequestMessageProperty();
                requestMessage.Headers["X-PAYPAL-SECURITY-USERID"] = _settings.PayPalSettings.PayPalSecurityUserId;
                requestMessage.Headers["X-PAYPAL-SECURITY-PASSWORD"] = _settings.PayPalSettings.PayPalSecurityPassword;
                requestMessage.Headers["X-PAYPAL-SECURITY-SIGNATURE"] = _settings.PayPalSettings.PayPalSecuritySignature;

                OperationContext.Current.OutgoingMessageProperties[HttpRequestMessageProperty.Name] = requestMessage;

                return client.SetTransactionContextAsync(request);
            }
        }

        private SetTransactionContextRequest CreateTransactionRequest(commonModel.ITransactionRequest transactionRequest, string userId)
        {
            var request = new SetTransactionContextRequest();
            request.requestEnvelope = new RequestEnvelope();
            if (transactionRequest.RequestEnvelope != null)
            {
                request.requestEnvelope.errorLanguage = transactionRequest.RequestEnvelope.ErrorLanguage;
            }

            if(string.IsNullOrEmpty(userId))
            {
                throw new FraudDataProviderException(FraudDataProviderErrors.InvalidUserIdCode, FraudDataProviderErrors.InvalidUserIdMessage);
            }
            var sender = _paypalDataProvider.GetPayPalFraudCheckDataById(userId);
            if (sender == null)
            {
                throw new FraudDataProviderException(FraudDataProviderErrors.InvalidUserIdCode, FraudDataProviderErrors.InvalidUserIdMessage);
            }
            request.trackingId = transactionRequest.TrackingId;
            request.senderAccount = new Account();
            //request.senderAccount.paypalAccountId = new PaypalAccountIdentifier();
            //request.senderAccount.paypalAccountId.payerId = sender.PayerId;
            request.senderAccount.partnerAccount = new PartnerAccount();
            request.senderAccount.partnerAccount.email = sender.EmailAddress;
            request.senderAccount.partnerAccount.phone = sender.PhoneNumber;
            request.senderAccount.partnerAccount.firstName = sender.FirstName;
            request.senderAccount.partnerAccount.lastName = sender.LastName;
            request.senderAccount.partnerAccount.createDate = sender.CreateDate ?? DateTime.Now;
            request.senderAccount.partnerAccount.lastGoodTransactionDate = sender.LastGoodTransactionDate;
            request.senderAccount.partnerAccount.transactionCountThreeMonths = sender.TransactionCountThreeMonths;

            request.senderAccount.partnerAccount.lastGoodTransactionDateSpecified = true;
            request.senderAccount.partnerAccount.createDateSpecified = true;
            request.senderAccount.partnerAccount.transactionCountThreeMonthsSpecified = true;

            request.ipAddress = new IpAddress();
            if (transactionRequest.IpAddress != null)
            {
                request.ipAddress.ipAddress = transactionRequest.IpAddress.IPAddress;
            }
            request.additionalData = new Pair[2];

            if (transactionRequest.SubOrders != null && transactionRequest.SubOrders.Length > 0)
            {
                request.subOrders = new SubOrder[transactionRequest.SubOrders.Length];

                var orders =
                    transactionRequest.SubOrders.ToList().ConvertAll(ConversionExtensions.ConvertToProxySubOrder);
                request.subOrders = orders.ToArray();
            }
            request.additionalData[0] = new Pair { key = "AverageTxnAmount", value = sender.AverageTxnAmount.ToString() };
            request.additionalData[1] = new Pair { key = "TxnFrequencyInDays", value = sender.TxnFrequencyInDays.ToString() };
            return request;
        }
    }
}
