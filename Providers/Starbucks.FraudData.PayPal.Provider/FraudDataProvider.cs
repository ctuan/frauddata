﻿using Starbucks.FraudData.Provider.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Starbucks.FraudData.Provider.Common.Models;
namespace Starbucks.FraudData.Provider
{
    public class FraudDataProvider : IFraudDataProvider
    {
        private readonly ICyberSourceFraudDataProvider _CSFraudDataProvider;

        public FraudDataProvider(ICyberSourceFraudDataProvider CSFraudDataProvider)
        {
            _CSFraudDataProvider = CSFraudDataProvider;
        }

        public IFraudCheckData GetMerchantData(string userId, int? cardId, MerchantType merchantType)
        {
            switch (merchantType)
            {
                case MerchantType.CyberSource:
                   // return _CSFraudDataProvider.GetUserDataForFraudCheck(userId, cardId);
                case MerchantType.CashStar:
                    break;
                case MerchantType.Paypal:
                    break;
            }

            return null;
        }
    }
}
