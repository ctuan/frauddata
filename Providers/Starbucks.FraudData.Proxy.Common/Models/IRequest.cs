﻿using System.Collections.Generic;
namespace Starbucks.FraudData.Proxy.Common.Models
{
    public interface IRequest
    {
        IEnumerable<IApplicationReply> ApplicationReplies { get; set; }
        IBillTo BillTo { get; set; }
        IEnumerable<ILineItem> LineItems { get; set; }
        IMerchantDefinedData MerchantDefinedData { get; set; }
        string MerchantReferenceNumber { get; set; }
        IPaymentData PaymentData { get; set; }
        IEnumerable<IPaymentMethod> PaymentMethod { get; set; }
        string PredecessorRequestID { get; set; }
        IEnumerable<IProfile> ProfileList { get; set; }
        string RequestDate { get; set; }
        string RequestID { get; set; }
        IRiskData RiskData { get; set; }
        IShipTo ShipTo { get; set; }
        string Source { get; set; }
        string SubscriptionID { get; set; }
        string TransactionReferenceNumber { get; set; }
    }
}
