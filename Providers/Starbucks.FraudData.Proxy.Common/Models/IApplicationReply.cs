﻿using System;
namespace Starbucks.FraudData.Proxy.Common.Models
{
    public interface IApplicationReply
    {
        string Name { get; set; }
        string RCode { get; set; }
        string RFlag { get; set; }
        string RMsg { get; set; }
    }
}
