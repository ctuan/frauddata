﻿namespace Starbucks.FraudData.Proxy.Common.Models
{
    public interface IOrderRequest
    {
        string MerchantId { get; set; }
        string Type { get; set; }
        string SubType { get; set; }
        /// <summary>
        /// Must be of format YYYYMMDD
        /// </summary>
        string TargetDate { get; set; }
        string MerchantReferenceNumber { get; set; }
        string VersionNumber { get; set; }
        string Username { get; set; }
        string Password { get; set; }
        string RequestId { get; set; }

    }
}
