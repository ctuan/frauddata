﻿using System;
namespace Starbucks.FraudData.Proxy.Common.Models
{
    public interface IBillTo
    {
        string Address1 { get; set; }
        string City { get; set; }
        string Country { get; set; }
        string CustomerID { get; set; }
        string Email { get; set; }
        string FirstName { get; set; }
        string Hostname { get; set; }
        string IPAddress { get; set; }
        string LastName { get; set; }
        string Phone { get; set; }
        string State { get; set; }
        string Zip { get; set; }
    }
}
