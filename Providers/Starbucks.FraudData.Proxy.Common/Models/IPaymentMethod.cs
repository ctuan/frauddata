﻿namespace Starbucks.FraudData.Proxy.Common.Models
{
   public interface IPaymentMethod
    {
        string AccountSuffix { get; set; }
        string CardType { get; set; }
        string ExpirationMonth { get; set; }
        string ExpirationYear { get; set; }
    }
}
