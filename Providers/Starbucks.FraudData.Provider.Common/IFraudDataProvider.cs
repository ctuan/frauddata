﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Starbucks.FraudData.Provider.Common.Models;

namespace Starbucks.FraudData.Provider.Common
{
    public interface IFraudDataProvider
    {
      IFraudCheckData GetMerchantData(IClientData clientData, MerchantType merchantType);
    }
}
