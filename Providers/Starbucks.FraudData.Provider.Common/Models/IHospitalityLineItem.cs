﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;

namespace Starbucks.FraudData.Provider.Common.Models
{      
    public interface IHospitalityLineItem : ICommonLineItem
    {
        DateTime arrivalDate { get; set; }

        DateTime DepartureDate { get; set; }

        int numberOfAdults { get; set; }

        int NumberOfChildren { get; set; }

        HospitalityLineItemFacilityRatingEnum FacilityRating { get; set; }

        int RoomCapacity { get; set; }
   
    }
}
