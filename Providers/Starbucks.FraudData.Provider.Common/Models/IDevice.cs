﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;

namespace Starbucks.FraudData.Provider.Common.Models
{
    public interface IDevice
    {
        string Id { get; set; }

        string LastSeenIpAddress { get; set; }

        DeviceTypeEnum Type { get; set; }

        string OperatingSystem { get; set; }

        string UserAgent { get; set; }

        DateTime FirstSeen { get; set; }

        DateTime LastSeen { get; set; }

        int SeenCount { get; set; }

    }
}
