﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Starbucks.FraudData.Provider.Common.Models
{
    public interface IMDDField
    {
        string ID { get; set; }
        string Value { get; set; }
    }
}
