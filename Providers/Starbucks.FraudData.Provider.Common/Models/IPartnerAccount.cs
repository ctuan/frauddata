﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;

namespace Starbucks.FraudData.Provider.Common.Models
{
    public interface IPartnerAccount
    {
        string AccountId { get; set; }

        string email { get; set; }

        string Phone { get; set; }

        string FirstName { get; set; }

        string LastName { get; set; }

        DateTime DateOfBirth { get; set; }

        IBaseAddress MailingAddress { get; set; }

        DateTime CreateDate { get; set; }

        /// <summary>
        /// The last date of a valid transaction. The transaction need not be a PayPal transaction.
        /// </summary>
        DateTime LastGoodTransactionDate { get; set; }

        /// <summary>
        /// Total transaction count for this partner account.
        /// </summary>
        int TransactionCountTotal { get; set; }

        /// <summary>
        /// Total chargeback count for this partner account.
        /// </summary>
        int ChargebackCountTotal { get; set; }

        /// <summary>
        /// Transaction count in the last three months for this partner account.
        /// </summary>
        int TransactionCountThreeMonths { get; set; }

        /// <summary>
        /// Chargeback count in the last three months for this partner account.
        /// </summary>
        int ChargebackCountThreeMonths { get; set; }
    }
}
