﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;

namespace Starbucks.FraudData.Provider.Common.Models
{
    public interface IShippingInfo
    {
        string FirstName { get; set; }

        string LastName { get; set; }

        IBaseAddress BaseAddress { get; set; }

        bool NewAddress { get; set; }

        DateTime? createdAt { get; set; }

        bool PreviousChargeback { get; set; }

    }
}
