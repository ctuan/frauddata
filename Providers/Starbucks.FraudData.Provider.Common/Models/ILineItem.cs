﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;

namespace Starbucks.FraudData.Provider.Common.Models
{
    public interface ILineItem
    {
        IRetailLineItem RetailLineItem { get; set; }

        IDgLineItem DgLineItem { get; set; }

        ITicketingLineItem TicketingLineItem { get; set; }

        ITelcoLineItem TelcoLineItem { get; set; }

        ITravelLineItem TravelLineItem { get; set; }

        IHospitalityLineItem HospitalityLineItem { get; set; }

    }
}
