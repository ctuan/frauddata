﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Web;

namespace Starbucks.FraudData.Provider.Common.Models
{
    public interface ITransactionResponse
    {
        /// <summary>
        ///  Application level acknowledgement code.
        /// </summary>
         IResponseEnvelope ResponseEnvelope { get; set; }

        /// <summary>
        ///   A unique ID that you specify to track the payment. NOTE: You are responsible for ensuring that the ID is unique.
        /// </summary>
        string TrackingId { get; set; }

        /// <summary>
        ///  Contains information about the sender's account
        /// </summary>
        IAccount senderAccount { get; set; }

        /// <summary>
        /// Contains information about the receiver's account. When the API caller is the receiver, this field is not needed.
        /// </summary>
        IAccount receiverAccount { get; set; }

        /// <summary>
        ///  Order information for this transaction.
        /// </summary>
        ISubOrder SubOrders { get; set; }

        /// <summary>
        ///  Device data.
        /// </summary>
        IDevice Device { get; set; }

        /// <summary>
        /// IP address for this transaction.
        /// </summary>
        IIpAddress IpAddress { get; set; }

        /// <summary>
        /// A list of merchant specific data for the transaction.
        /// </summary>
        IPair[] AdditionalData { get; set; }
    }
}