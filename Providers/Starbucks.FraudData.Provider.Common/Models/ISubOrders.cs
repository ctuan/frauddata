﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;

namespace Starbucks.FraudData.Provider.Common.Models
{
    public interface ISubOrder
    {
        ILineItem[] LineItems { get; set; }

        IShippingInfo ShippingInfo { get; set; }

        DeliveryEnum Delivery { get; set; }
    }
}
