﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace Starbucks.FraudData.Provider.Common.Models
{
    public interface IResponseEnvelope
    {
        DateTime Timestamp { get; set; }

        AckCode Ack { get; set; }

        string CorrelationId { get; set; }

        string Build { get; set; }

        object Other { get; set; } 
    }
}
