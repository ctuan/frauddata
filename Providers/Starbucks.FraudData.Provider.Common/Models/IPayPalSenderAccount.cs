﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Starbucks.FraudData.Provider.Common.Models
{
    public interface IPayPalSenderAccount
    {
        string PayerId { get; set; }

        string EmailAddress { get; set; }

        string  FirstName { get; set; }

        string LastName { get; set; }

        DateTime? CreateDate { get; set; }

        string PhoneNumber { get; set; }
    }
}
