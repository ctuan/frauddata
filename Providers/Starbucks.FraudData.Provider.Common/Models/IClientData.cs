﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Starbucks.FraudData.Provider.Common.Models
{
    public interface IClientData
    {
        string UserId { get; set; }
        string CardId { get; set; }
        string OrderSource { get; set; }
        string PurchaserIpAddress { get; set; }
        string PaymentMethodNickName { get; set; }
        bool IsAutoReload { get; set; }
        string PaypalPayerId { get; set; }
        decimal PurchaserSessionDuration { get; set; }
        IClientMDDData[] ClientMDDData { get; set; }
    }
}
