﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Starbucks.FraudData.Provider.Common.Models
{
    public enum MerchantType
    {
        CyberSource,
        Paypal,
        CashStar
    }
}
