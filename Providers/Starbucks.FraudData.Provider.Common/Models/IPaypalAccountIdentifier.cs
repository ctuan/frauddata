﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;

namespace Starbucks.FraudData.Provider.Common.Models
{
    public interface IPaypalAccountIdentifier
    {
        /// <summary>
        /// The PayPal payer ID, which uniquely identifies a PayPal account.
        /// </summary>
        string PayerId { get; set; }
    }
}
