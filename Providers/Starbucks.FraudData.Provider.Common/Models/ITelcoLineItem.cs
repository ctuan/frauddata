﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;

namespace Starbucks.FraudData.Provider.Common.Models
{
    public interface ITelcoLineItem : ICommonLineItem
    {
        string phoneNumber { get; set; }

        TelcoLineItemOriginEnum Origin { get; set; }

        string ResellerID { get; set; }


    }
}
