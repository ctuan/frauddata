﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;

namespace Starbucks.FraudData.Provider.Common.Models
{
    public interface ITravelLineItem : ICommonLineItem
    {
        TravelLineItemTypeEnum type { get; set; }

        string Origin { get; set; }

        string Destination { get; set; }

        DateTime DepartureDate { get; set; }

        string Operator { get; set; }

        string TripIdentifier { get; set; }

        TravelLineItemClassEnum TravelClass { get; set; }

        IList<IPartnerAccount> Passengers { get; set; }

        TravelLineItemReasonEnum reason { get; set; }

        TravelLineItemBookingTypeEnum BookingType { get; set; }
   
    }
}
