﻿using Starbucks.FraudData.Provider.Common.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Starbucks.FraudData.Provider.Common
{
    public interface ICyberSourceFraudDataProvider
    {
        IFraudCheckData GetUserDataForFraudCheck(Dal.Common.Models.IClientData clientData);
    }
}
