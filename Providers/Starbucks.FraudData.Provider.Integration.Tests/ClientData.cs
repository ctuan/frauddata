﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Starbucks.FraudData.Dal.Common.Models;

namespace Starbucks.FraudData.Provider.Integration.Tests
{
    public class ClientData : IClientData
    {
        public string UserId
        {
            get;
            set;
        }

        public string CardId
        {
            get;
            set;

        }

        public string OrderSource
        {
            get;
            set;

        }

        public string PurchaserIpAddress
        {
            get;
            set;

        }

        public string PaymentMethodNickName
        {
            get;
            set;
        }

        public bool IsAutoReload
        {
            get;
            set;
        }

        public string PaypalPayerId
        {
            get;
            set;
        }

        public decimal PurchaserSessionDuration
        {
            get;
            set;
        }

        public IClientMDDData[] ClientMDDData
        {
            get;
            set;
        }
    }
}
