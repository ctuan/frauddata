﻿using System.Linq.Expressions;
using Starbucks.FraudData.Dal.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using System.Data.SqlClient;
using Starbucks.FraudData.Dal.Sql.Configuration;
using System.Configuration;
using Starbucks.FraudData.Dal.Common.Models;
using Starbucks.FraudData.Dal.Sql.Models;
using Starbucks.FraudData.Provider;
using Starbucks.TransactionHistory.Dal.Common;
using ImpromptuInterface;
using Starbucks.TransactionHistory.Common.Models;
using Starbucks.TransactionHistory.Common.Enums;

namespace Starbucks.FraudData.Dal.Sql
{
    public class PayPalFraudCheckData : IPayPalFraudCheckData
    {
        private readonly string _connectionString;
        private readonly ITransactionHistoryDataProvider _transactionHistoryDataProvider;
        private readonly FraudDataProviderSettings _settings;
        public PayPalFraudCheckData(FraudDataDalSqlSettingsSection fraudDataDalSqlSettingsSection, ITransactionHistoryDataProvider transactionHistoryDataProvider, FraudDataProviderSettings settings)
        {
            _connectionString = ConfigurationManager.ConnectionStrings[fraudDataDalSqlSettingsSection.ConnectionStringName].ConnectionString;
            _transactionHistoryDataProvider = transactionHistoryDataProvider;
            _settings = settings;
        }

        public IPayPalSenderAccount GetPayPalFraudCheckDataById(string userId)
        {
            IPayPalSenderAccount sender = null;
            using (var connection = new SqlConnection(_connectionString))
                try
                {
                    using (var dbCommand = GetCommand("GetPayPalFraudCheckDataById", connection))
                    {
                        connection.Open();
                        dbCommand.Parameters.AddWithValue("@UserId", userId);
                        using (IDataReader reader = dbCommand.ExecuteReader())
                        {
                            if (reader.Read())
                            {
                                sender = new PayPalSenderAccount
                                {
                                    PayerId = reader.GetStringExt("PayPalPayerId"),
                                    FirstName = reader.GetStringExt("u_first_name"),
                                    LastName = reader.GetStringExt("u_last_name"),
                                    EmailAddress = reader.GetStringExt("u_email_address"),
                                    PhoneNumber = reader.GetStringExt("u_tel_number"),
                                    CreateDate = reader.GetDateTimeExt("d_date_created")
                                };
                            }
                        }
                    }
                }
                catch (SqlException ex)
                {
                    var c = ex.Message;
                }
            if (sender == null)
            {
                return null;
            }
            var enddate = DateTime.Now;
            var startDate = DateTime.Now.AddDays(-1 * _settings.TransactionHistoryPeriodToPaypal);// need to get data of three months
            var pageRequest = new { Offset = 0, Limit = int.MaxValue };
            var page = pageRequest.ActLike<IPagedRequest>();
            var transactionHistory = _transactionHistoryDataProvider.GetHistoryItemsByDateRange(userId, string.Empty, startDate, enddate, null, page);

            var lastReloadTransaction = (from n in transactionHistory
                                         where n.TransactionDetails != null && n.TransactionDetails.TransactionType == TransactionType.Reload
                                         orderby n.TransactionDetails.LocalTime descending
                                         select n).FirstOrDefault();

            if (lastReloadTransaction != null)
            {
                var lastGoodTransactionDate = lastReloadTransaction.TransactionDetails.LocalTime;
                sender.LastGoodTransactionDate = lastGoodTransactionDate;
            }
            //var transactionCount = transactionHistory.Count();// Currently we only store history for 45 days.
            var transactionCount = (from n in transactionHistory
                                    where
                                        n.TransactionDetails != null &&
                                        n.TransactionDetails.TransactionType == TransactionType.Reload
                                    select n).Count();
            // Question: Do we need to subtract void reload transactions from the total?

            sender.TransactionCountThreeMonths = transactionCount;
            var listOfTotal = (from n in transactionHistory
                               where n.TransactionDetails != null && n.TransactionDetails.TransactionType == TransactionType.Reload
                               orderby n.TransactionDetails.LocalTime descending
                               select n.TotalAmount);

            var averageTxnAmount = (transactionCount > 0) ? (listOfTotal.Sum() / transactionCount) : 0;
            sender.AverageTxnAmount = Math.Round(averageTxnAmount ?? 0M, 2);
            var txnFrequencyInDays = (transactionCount > 0) ? Math.Round((transactionCount / (double)_settings.TransactionHistoryPeriodToPaypal),2) : 0;
            sender.TxnFrequencyInDays = txnFrequencyInDays;
            return sender;
        }

        private SqlCommand GetCommand(string sproc, SqlConnection connection)
        {
            return new SqlCommand(sproc, connection) { CommandType = CommandType.StoredProcedure };
        }

        public IPagedRequest IpageRequest { get; set; }
    }
}
