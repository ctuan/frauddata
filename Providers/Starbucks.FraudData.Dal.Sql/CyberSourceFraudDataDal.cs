﻿using Starbucks.FraudData.Dal.Common;
using Starbucks.FraudData.Dal.Common.Models;
using Starbucks.FraudData.Dal.Sql.Configuration;
using Starbucks.FraudData.Dal.Sql.Models;
using Starbucks.Platform.Security;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Starbucks.FraudData.Dal.Sql
{
    public class CyberSourceFraudDataDal : ICyberSourceFraudDataDal
    {
        private readonly string _connectionString;

        public CyberSourceFraudDataDal(FraudDataDalSqlSettingsSection fraudDataDalSqlSettingsSection)
        {
            _connectionString = ConfigurationManager.ConnectionStrings[fraudDataDalSqlSettingsSection.ConnectionStringName].ConnectionString;
        }

        #region Public Methods

        public IFraudCheckData GetUserDataForFraudCheck(IClientData clientData, IFraudCheckData result)
        {
            using (var connection = new SqlConnection(_connectionString))
            using (var dbCommand = GetCommand("GetCompleteUserDataForFraudCheck", connection))
            {
                if (clientData == null)
                    return new FraudCheckData();
                connection.Open();
                dbCommand.Parameters.AddWithValue("@UserId", clientData.UserId);
                dbCommand.Parameters.AddWithValue("@CardId",string.IsNullOrEmpty(clientData.CardId) ? "" : Encryption.DecryptCardId(clientData.CardId).ToString());

                try
                {
                    using (IDataReader reader = dbCommand.ExecuteReader())
                    {
                        if (reader.Read())
                        {
                            result.UserId = reader.GetStringExt("UserId");
                            result.EmailAddress = reader.GetStringExt("EmailAddress");
                            result.UserName = reader.GetStringExt("UserName");
                            result.IsUserSignedIn = !String.IsNullOrEmpty(result.UserId);
                            result.AgeOfAccountSinceCreation = reader.GetInt32Ext("AgeOfAccountSinceCreation") ?? 0;
                            result.IsPartner = (reader.GetInt32Ext("IsPartner") ?? 0) == 1;
                            result.BirthDay = reader.GetInt32Ext("BirthDay") ?? 0;
                            result.BirthMonth = reader.GetInt32Ext("BirthMonth") ?? 0;
                            result.eMailSignUp = reader.GetBoolExt("eMailSignUp") ?? false;
                            result.MailSignUp = reader.GetBoolExt("mailSignUp") ?? false;
                            result.TextMessageSignUp = (reader.GetInt32Ext("TextMessageSignUp") ?? 0) == 1;
                            result.FacebookSignUp = (reader.GetInt32Ext("FacebookSignUp") ?? 0) == 1;
                            result.CardRegistrationDate = reader.GetDateTimeExt("RegistrationDate");
                            result.TotalCardsRegistered = reader.GetInt32Ext("TotalCardsRegistered") ?? 0;
                            result.SvcNumberReloaded = reader.GetStringExt("SvcNumber") ?? string.Empty;
                            result.BalancePriorToReload = reader.GetDecimalExt("Balance") ?? 0.0M;
                            result.IsAutoreload = clientData.IsAutoReload;
                            result.PaypalPayerId = clientData.PaypalPayerId ?? "";
                            result.OrderSource = clientData.OrderSource;
                            result.PurchaserIpAddress = clientData.PurchaserIpAddress;
                            result.IsCardCurrentlyRegisteredToUser = result.UserId == reader.GetStringExt("RegisteredUserId");
                            result.MDDFields[0] = new MDDField { ID = "21", Value = reader.GetStringExt("CountryOfOriginOfSvcNumber") };
                            result.MDDFields[1] = new MDDField { ID = "22", Value = reader.GetInt32Ext("TotalCardsAssociated").ToString() };
                            result.MDDFields[2] = new MDDField { ID = "23", Value = reader.GetBoolExt("UserOptedForInterests").ToString() };
                            result.MDDFields[3] = new MDDField { ID = "24", Value = reader.GetDateTimeExt("InterestsLastModifiedDate").ToString() };
                            result.MDDFields[4] = new MDDField { ID = "25", Value = reader.GetInt32Ext("NoOfCodesSentForBT").ToString() };
                            result.MDDFields[5] = new MDDField { ID = "26", Value = reader.GetInt32Ext("NoOfCodesSentForEV").ToString() };
                            result.MDDFields[6] = new MDDField { ID = "27", Value = reader.GetInt32Ext("NoOfDaysVerifiedForBT").ToString() };
                            result.MDDFields[7] = new MDDField { ID = "28", Value = reader.GetInt32Ext("NoOfDaysVerifiedForEV").ToString() };
                            result.MDDFields[8] = new MDDField { ID = "29", Value = reader.GetStringExt("TwitterSignUp") };
                            result.MDDFields[9] = new MDDField { ID = "30", Value = reader.GetBoolExt("IsDefault").ToString() };
                            result.MDDFields[10] = new MDDField { ID = "31", Value = reader.GetBoolExt("IsAssociatedCardReloaded").ToString() };
                            result.MDDFields[11] = new MDDField { ID = "32", Value = reader.GetStringExt("AssociatedCardUserId") };
                            result.MDDFields[12] = new MDDField() { ID = "33", Value = clientData.PaymentMethodNickName };
                            result.MDDFields[13] = new MDDField() { ID = "34", Value = clientData.PurchaserSessionDuration.ToString() };
                        }
                    }
                }
                catch (SqlException ex)
                {
                    throw ex;
                }
            }
            return result;
        }

      
        #endregion

        #region Private Methods

        private SqlCommand GetCommand(string sproc, SqlConnection connection)
        {
            return new SqlCommand(sproc, connection) { CommandType = CommandType.StoredProcedure };
        }

        #endregion
    }
}
