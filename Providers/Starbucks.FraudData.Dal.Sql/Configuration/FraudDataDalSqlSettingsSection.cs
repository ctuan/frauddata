﻿using System.Configuration;

namespace Starbucks.FraudData.Dal.Sql.Configuration
{
    public class FraudDataDalSqlSettingsSection : ConfigurationSection
    {
        [ConfigurationProperty("connectionStringName")]
        public string ConnectionStringName
        {
            get { return (string)this["connectionStringName"]; }
            set { this["connectionStringName"] = value; }
        }
    }
}
