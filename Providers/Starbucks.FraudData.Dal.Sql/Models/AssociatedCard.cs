﻿using Starbucks.FraudData.Dal.Common.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Starbucks.FraudData.Dal.Sql.Models
{
    public class AssociatedCard : IAssociatedCard
    {
       public string SubMarketCode { get; set; }
       public bool IsDefault { get; set; }
       public bool IsOwner { get; set; }
       public string Nickname { get; set; }
       public string RegisteredUserId { get; set; }
       public DateTime? RegistrationDate { get; set; }
       public string RegistrationSource { get; set; }
       public string Number { get; set; }
       public decimal? Balance { get; set; }
    }
}
