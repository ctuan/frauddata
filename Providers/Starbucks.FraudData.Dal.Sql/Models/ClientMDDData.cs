﻿using Starbucks.FraudData.Dal.Common.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Starbucks.FraudData.Dal.Sql.Models
{
    public class ClientMDDData : IClientMDDData
    {
        public string ID { get; set; }
        public string Value { get; set; }
    }
}
