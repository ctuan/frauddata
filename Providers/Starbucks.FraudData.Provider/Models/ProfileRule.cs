﻿using Starbucks.FraudData.Proxy.Common.Models;

namespace Starbucks.FraudData.Provider.Models
{
    public class ProfileRule : IProfileRule
    {
        public string RuleName { get; set; }

        public string RuleDecision { get; set; }
    }
}
