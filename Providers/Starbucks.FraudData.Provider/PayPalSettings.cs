using System;
using System.Configuration;

namespace Starbucks.FraudData.Provider
{
    public class PayPalSettings : ConfigurationElement
    {
        [ConfigurationProperty("payPalSecurityUserId", IsRequired = true)]
        public string PayPalSecurityUserId
        {
            get
            {
                return this["payPalSecurityUserId"].ToString();
            }
            set
            {
                this["payPalSecurityUserId"] = value;
            }
        }

        [ConfigurationProperty("payPalSecurityPassword", IsRequired = true)]
        public string PayPalSecurityPassword
        {
            get
            {
                return this["payPalSecurityPassword"].ToString();
            }
            set
            {
                this["payPalSecurityPassword"] = value;
            }
        }

        [ConfigurationProperty("payPalSecuritySignature", IsRequired = true)]
        public string PayPalSecuritySignature
        {
            get
            {
                return this["payPalSecuritySignature"].ToString();
            }
            set
            {
                this["payPalSecuritySignature"] = value;
            }
        }

        [ConfigurationProperty("logFraudCheckDataForPayPal", DefaultValue = "false", IsRequired = false)]
        public Boolean LogFraudCheckDataForPayPal
        {
            get
            {
                return (Boolean)this["logFraudCheckDataForPayPal"];
            }
            set
            {
                this["logFraudCheckDataForPayPal"] = value;
            }
        }
    }
}