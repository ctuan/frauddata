﻿using System.Xml.Serialization;

namespace Starbucks.FraudData.Provider.DataContracts
{
    public class LineItem
    {
        public string FulfillmentType { get; set; }

        public string Quantity { get; set; }

        public string UnitPrice { get; set; }

        public string TaxAmount { get; set; }

        public string MerchantProductSKU { get; set; }

        public string ProductName { get; set; }

        public string ProductCode { get; set; }

        public string Number { get; set; }
    }
}
