﻿using System.Xml.Serialization;

namespace Starbucks.FraudData.Provider.DataContracts
{
    [XmlRoot("Report", Namespace = "https://ebctest.cybersource.com/ebctest/reports/dtd/tdr_1_3.dtd")]
    public class Report
    {
        [XmlArrayItem("Request", typeof(Request))]
        public Request[] Requests { get; set; }

        [XmlAttribute]
        public string Name { get; set; }

        [XmlAttribute]
        public string Version { get; set; }

        [XmlAttribute]
        public string MerchantID { get; set; }

        [XmlAttribute]
        public string ReportStartDate { get; set; }

        [XmlAttribute]
        public string ReportEndDate { get; set; }
    }
}
