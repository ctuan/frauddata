﻿using System.Xml.Serialization;

namespace Starbucks.FraudData.Provider.DataContracts
{
    public class BillTo
    {
        public string FirstName { get; set; }

        public string LastName { get; set; }

        public string Address1 { get; set; }

        public string City { get; set; }

        public string State { get; set; }

        public string Zip { get; set; }

        public string Email { get; set; }

        public string Country { get; set; }

        public string Phone { get; set; }

        public string IPAddress { get; set; }

        public string Hostname { get; set; }

        public string CustomerID { get; set; }
    }
}
