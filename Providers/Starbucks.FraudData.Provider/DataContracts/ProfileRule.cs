﻿using System.Xml.Serialization;

namespace Starbucks.FraudData.Provider.DataContracts
{
    public class ProfileRule
    {
        public string RuleName { get; set; }

        public string RuleDecision { get; set; }
    }
}
