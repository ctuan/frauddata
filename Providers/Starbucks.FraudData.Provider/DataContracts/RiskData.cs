﻿using System.Xml.Serialization;

namespace Starbucks.FraudData.Provider.DataContracts
{
    public class RiskData
    {
        public string Factors { get; set; }

        public string HostSeverity { get; set; }

        public string Score { get; set; }

        public string TimeLocal { get; set; }

        public string AppliedThreshold { get; set; }

        public string AppliedTimeHedge { get; set; }

        public string AppliedVelocityHedge { get; set; }

        public string AppliedHostHedge { get; set; }

        public string AppliedCategoryGift { get; set; }

        public string AppliedCategoryTime { get; set; }

        public string AppliedAVS { get; set; }
    }
}
