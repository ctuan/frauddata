﻿using System.Xml.Serialization;

namespace Starbucks.FraudData.Provider.DataContracts
{
    public class MerchantDefinedData
    {
        [XmlElement("field1")]
        public string Field1 { get; set; }

        [XmlElement("field2")]
        public string Field2 { get; set; }

        [XmlElement("field3")]
        public string Field3 { get; set; }

        [XmlElement("field4")]
        public string Field4 { get; set; }

        [XmlElement("field5")]
        public string Field5 { get; set; }

        [XmlElement("field6")]
        public string Field6 { get; set; }

        [XmlElement("field7")]
        public string Field7 { get; set; }

        [XmlElement("field8")]
        public string Field8 { get; set; }

        [XmlElement("field9")]
        public string Field9 { get; set; }

        [XmlElement("field10")]
        public string Field10 { get; set; }

        [XmlElement("field11")]
        public string Field11 { get; set; }

        [XmlElement("field12")]
        public string Field12 { get; set; }

        [XmlElement("field13")]
        public string Field13 { get; set; }

        [XmlElement("field14")]
        public string Field14 { get; set; }

        [XmlElement("field15")]
        public string Field15 { get; set; }

        [XmlElement("field16")]
        public string Field16 { get; set; }

        [XmlElement("field17")]
        public string Field17 { get; set; }

        [XmlElement("field18")]
        public string Field18 { get; set; }

        [XmlElement("field19")]
        public string Field19 { get; set; }

        [XmlElement("field20")]
        public string Field20 { get; set; }
    }
}
