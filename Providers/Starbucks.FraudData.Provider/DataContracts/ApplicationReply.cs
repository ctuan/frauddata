﻿using System.Xml.Serialization;

namespace Starbucks.FraudData.Provider.DataContracts
{
    public class ApplicationReply 
    {
        public string RCode { get; set; }

        public string RFlag { get; set; }

        public string RMsg { get; set; }

        [XmlAttribute]
        public string Name { get; set; }
    }
}
