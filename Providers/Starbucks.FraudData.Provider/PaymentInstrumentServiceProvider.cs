﻿using Starbucks.FraudData.Provider.Common;
using Starbucks.FraudData.Proxy.Common;
using Starbucks.FraudData.Proxy.Common.Models;
using System;
using System.Collections.Specialized;
using System.IO;
using System.Net;
using System.Threading.Tasks;
using System.Web;
using System.Xml;
using System.Xml.Serialization;
using Starbucks.FraudData.Provider.Extensions;

namespace Starbucks.FraudData.Provider
{
    public class PaymentInstrumentServiceProvider : IPaymentInstrumentServiceProvider
    {
        private IPaymentInstrumentServiceProxy _paymentInstrumentServiceProxy;

        public PaymentInstrumentServiceProvider(IPaymentInstrumentServiceProxy paymentInstrumentServiceProxy)
        {
            if (paymentInstrumentServiceProxy == null)
            {
                throw new ArgumentNullException("cannot be null", "paymentInstrumentServiceProxy");
            }
            _paymentInstrumentServiceProxy = paymentInstrumentServiceProxy;
        }

        public async Task<IReport> GetOrderDetail(IOrderRequest orderRequest)
        {
            if (orderRequest == null)
            {
                throw new ArgumentNullException("cannot be null", "orderRequest");
            }

            var credential = new NetworkCredential(orderRequest.Username, orderRequest.Password);
            string postData = GetRequestBody(orderRequest);
            string response = await _paymentInstrumentServiceProxy.GetOrderDetailAsync(credential, postData);

            DataContracts.Report report = await DeserializeTo<DataContracts.Report>(response);
            return report.ToIReport();

        }

        private string GetRequestBody(IOrderRequest request)
        {
            NameValueCollection outgoingQueryString = HttpUtility.ParseQueryString(String.Empty);
            outgoingQueryString.Add("merchantID", request.MerchantId);
            outgoingQueryString.Add("type", request.Type);
            outgoingQueryString.Add("subtype", request.SubType);
            outgoingQueryString.Add("targetDate", request.TargetDate);
            outgoingQueryString.Add("merchantReferenceNumber", request.MerchantReferenceNumber);
            outgoingQueryString.Add("versionNumber", request.VersionNumber);
            return outgoingQueryString.ToString();
        }

        private Task<T> DeserializeTo<T>(string xmlInput) where T : class
        {
            if (String.IsNullOrWhiteSpace(xmlInput))
            {
                throw new ArgumentNullException("cannot be null or empty", "xmlInput");
            }
            return Task.Run(() =>
            {
                using (var stringReader = new StringReader(xmlInput))
                {
                    using (XmlReader reader = new XmlTextReader(stringReader))
                    {
                        var serializer = new XmlSerializer(typeof(T));
                        return serializer.Deserialize(reader) as T;
                    }
                }
            });
        }

    }
}
