﻿using Starbucks.FraudData.Provider.Common.Models;
using Starbucks.FraudData.Provider.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Starbucks.FraudData.Provider
{
    public static class Converters
    {
        public static IFraudCheckData ToApi(this Dal.Common.Models.IFraudCheckData fraudCheckData)
        {
            return new FraudCheckData
            {
                OrderSource = fraudCheckData.OrderSource,
                SvcNumber = fraudCheckData.SvcNumber,
                CardRegistrationDate = fraudCheckData.CardRegistrationDate,
                SvcNumberReloaded = fraudCheckData.SvcNumberReloaded,
               // CallCenterRepId = fraudCheckData.CallCenterRepId,   // can be removed
                IsCardCurrentlyRegisteredToUser = fraudCheckData.IsCardCurrentlyRegisteredToUser,
               // ShippingMethod = fraudCheckData.ShippingMethod,   // can be removed
                IsUserSignedIn = fraudCheckData.IsUserSignedIn,
                UserName = fraudCheckData.UserName,
                EmailAddress = fraudCheckData.EmailAddress,
                PaypalPayerId = fraudCheckData.PaypalPayerId,
                UserId = fraudCheckData.UserId,
                AgeOfAccountSinceCreation = fraudCheckData.AgeOfAccountSinceCreation,
                IsPartner = fraudCheckData.IsPartner,
                BirthDay = fraudCheckData.BirthDay,   // combine with birth month in MM\DD format
                BirthMonth = fraudCheckData.BirthMonth,
                MailSignUp = fraudCheckData.MailSignUp,
                eMailSignUp = fraudCheckData.eMailSignUp,
                TextMessageSignUp = fraudCheckData.TextMessageSignUp,
               // TwitterSignUp = fraudCheckData.TwitterSignUp,
                FacebookSignUp = fraudCheckData.FacebookSignUp,
               // PurchaserSessionDuration = fraudCheckData.PurchaserSessionDuration,
                PurchaserIpAddress = fraudCheckData.PurchaserIpAddress,
                //CardBalance = fraudCheckData.CardBalance,
               // Currency = fraudCheckData.Currency,
                //Nickname = fraudCheckData.Nickname,
             //   IsDefaultSvcCard = fraudCheckData.IsDefaultSvcCard,
                TotalCardsRegistered = fraudCheckData.TotalCardsRegistered,
               // SessionId = fraudCheckData.SessionId,
                IsAutoreload = fraudCheckData.IsAutoreload,
                BalancePriorToReload = fraudCheckData.BalancePriorToReload,
                RewardTierLevel = fraudCheckData.RewardTierLevel,
                DaysSinceLastReload = fraudCheckData.DaysSinceLastReload,
                TimeSinceLastPOSTransaction = fraudCheckData.TimeSinceLastPOSTransaction,
                MDDFields = fraudCheckData.MDDFields
            };
        }
    }
}
