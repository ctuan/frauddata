﻿using Starbucks.FraudData.Proxy.Common.Models;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Starbucks.FraudData.Provider.Extensions
{
    public static class Mapper
    {
        public static IReport ToIReport(this DataContracts.Report report)
        {
            if (report == null)
            {
                return null;
            }
            return new Models.Report
             {
                 MerchantID = report.MerchantID,
                 Name = report.Name,
                 ReportEndDate = report.ReportEndDate,
                 ReportStartDate = report.ReportStartDate,
                 Version = report.Version,
                 Requests = report.Requests.Convert(r => r.ToIRequest())
             };
        }

        public static IRequest ToIRequest(this DataContracts.Request request)
        {
            if (request == null)
            {
                return null;
            }

            return new Models.Request
            {
                ApplicationReplies = request.ApplicationReplies.Convert(a => a.ToIApplicationReply()),
                BillTo = request.BillTo.ToIBillTo(),
                LineItems = request.LineItems.Convert(l => l.ToILineItem()),
                MerchantDefinedData = request.MerchantDefinedData.ToIMerchantDefinedData(),
                MerchantReferenceNumber = request.MerchantReferenceNumber,
                PaymentData = request.PaymentData.ToIPaymentData(),
                PaymentMethod = request.PaymentMethod.Convert(p => p.ToIPaymentMethod()),
                PredecessorRequestID = request.PredecessorRequestID,
                ProfileList = request.ProfileList.Convert(p => p.ToIProfile()),
                RequestDate = request.RequestDate,
                RequestID = request.RequestID,
                RiskData = request.RiskData.ToIRisKData(),
                ShipTo = request.ShipTo.ToIShipTo(),
                Source = request.Source,
                SubscriptionID = request.SubscriptionID,
                TransactionReferenceNumber = request.TransactionReferenceNumber
            };
        }

        public static IApplicationReply ToIApplicationReply(this DataContracts.ApplicationReply applicationReply)
        {
            if (applicationReply == null)
            {
                return null;
            }

            return new Models.ApplicationReply
            {
                Name = applicationReply.Name,
                RCode = applicationReply.RCode,
                RFlag = applicationReply.RFlag,
                RMsg = applicationReply.RMsg
            };
        }

        public static IBillTo ToIBillTo(this DataContracts.BillTo billTo)
        {
            if (billTo == null)
            {
                return null;
            }

            return new Models.BillTo
            {
                Address1 = billTo.Address1,
                City = billTo.City,
                Country = billTo.Country,
                CustomerID = billTo.CustomerID,
                Email = billTo.Email,
                FirstName = billTo.FirstName,
                Hostname = billTo.Hostname,
                IPAddress = billTo.IPAddress,
                LastName = billTo.LastName,
                Phone = billTo.Phone,
                State = billTo.State,
                Zip = billTo.Zip
            };
        }

        public static ILineItem ToILineItem(this DataContracts.LineItem lineItem)
        {
            if (lineItem == null)
            {
                return null;
            }

            return new Models.LineItem
            {
                FulfillmentType = lineItem.FulfillmentType,
                MerchantProductSKU = lineItem.MerchantProductSKU,
                Number = lineItem.Number,
                ProductCode = lineItem.ProductCode,
                ProductName = lineItem.ProductName,
                Quantity = lineItem.Quantity,
                TaxAmount = lineItem.TaxAmount,
                UnitPrice = lineItem.UnitPrice,
            };
        }

        public static IMerchantDefinedData ToIMerchantDefinedData(this DataContracts.MerchantDefinedData merchantDefinedData)
        {
            if (merchantDefinedData == null)
            {
                return null;
            }

            return new Models.MerchantDefinedData
              {
                  Field1 = merchantDefinedData.Field1,
                  Field2 = merchantDefinedData.Field2,
                  Field3 = merchantDefinedData.Field3,
                  Field4 = merchantDefinedData.Field4,
                  Field5 = merchantDefinedData.Field5,
                  Field6 = merchantDefinedData.Field6,
                  Field7 = merchantDefinedData.Field7,
                  Field8 = merchantDefinedData.Field8,
                  Field9 = merchantDefinedData.Field9,
                  Field10 = merchantDefinedData.Field10,
                  Field11 = merchantDefinedData.Field11,
                  Field12 = merchantDefinedData.Field12,
                  Field13 = merchantDefinedData.Field13,
                  Field14 = merchantDefinedData.Field14,
                  Field15 = merchantDefinedData.Field15,
                  Field16 = merchantDefinedData.Field16,
                  Field17 = merchantDefinedData.Field17,
                  Field18 = merchantDefinedData.Field18,
                  Field19 = merchantDefinedData.Field19,
                  Field20 = merchantDefinedData.Field20,
              };
        }

        public static IPaymentData ToIPaymentData(this DataContracts.PaymentData paymentData)
        {
            if (paymentData == null)
            {
                return null;
            }

            return new Models.PaymentData
            {
                Amount = paymentData.Amount,
                AuthorizationCode = paymentData.AuthorizationCode,
                AVSResult = paymentData.AVSResult,
                AVSResultMapped = paymentData.AVSResultMapped,
                CurrencyCode = paymentData.CurrencyCode,
                PaymentProcessor = paymentData.PaymentProcessor,
                PaymentRequestID = paymentData.PaymentRequestID,
                TotalTaxAmount = paymentData.TotalTaxAmount
            };
        }

        public static IPaymentMethod ToIPaymentMethod(this DataContracts.PaymentMethod paymentMethod)
        {
            if (paymentMethod == null)
            {
                return null;
            }

            return new Models.PaymentMethod
            {
                AccountSuffix = paymentMethod.AccountSuffix,
                CardType = paymentMethod.CardType,
                ExpirationMonth = paymentMethod.ExpirationMonth,
                ExpirationYear = paymentMethod.ExpirationYear
            };
        }

        public static IProfile ToIProfile(this DataContracts.Profile profile)
        {
            if (profile == null)
            {
                return null;
            }

            return new Models.Profile
            {
                Name = profile.Name,
                ProfileDecision = profile.ProfileDecision,
                ProfileMode = profile.ProfileMode,
                RuleList = profile.RuleList.Convert(r => r.ToIProfileRule())
            };
        }

        public static IProfileRule ToIProfileRule(this DataContracts.ProfileRule profileRule)
        {
            if (profileRule == null)
            {
                return null;
            }

            return new Models.ProfileRule
            {
                RuleDecision = profileRule.RuleDecision,
                RuleName = profileRule.RuleName
            };
        }

        public static IRiskData ToIRisKData(this DataContracts.RiskData riskData)
        {
            if (riskData == null)
            {
                return null;
            }

            return new Models.RiskData
            {
                AppliedAVS = riskData.AppliedAVS,
                AppliedCategoryGift = riskData.AppliedCategoryGift,
                AppliedCategoryTime = riskData.AppliedCategoryTime,
                AppliedHostHedge = riskData.AppliedHostHedge,
                AppliedThreshold = riskData.AppliedThreshold,
                AppliedTimeHedge = riskData.AppliedTimeHedge,
                AppliedVelocityHedge = riskData.AppliedVelocityHedge,
                Factors = riskData.Factors,
                HostSeverity = riskData.HostSeverity,
                Score = riskData.Score,
                TimeLocal = riskData.TimeLocal
            };
        }

        public static IShipTo ToIShipTo(this DataContracts.ShipTo shipTo)
        {
            if (shipTo == null)
            {
                return null;
            }

            return new Models.ShipTo
            {
                Phone = shipTo.Phone,
                FirstName = shipTo.FirstName,
                LastName = shipTo.LastName,
                Address1 = shipTo.Address1,
                Address2 = shipTo.Address2,
                City = shipTo.City,
                State = shipTo.State,
                Zip = shipTo.Zip,
                CompanyName = shipTo.CompanyName,
                Country = shipTo.Country,
            };
        }


        public static IEnumerable<TResult> Convert<TSource, TResult>(this IEnumerable<TSource> source, Func<TSource, TResult> selector)
        {
            if (source == null)
            {
                return new List<TResult>();
            }
            return source.Select(selector);
        }

    }
}
